package {
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	[Frame(factoryClass="Preloader")]
	public class Main extends Engine {
		public static const DEBUG:Boolean = false;
		
		public function Main() {
			super(400, 300, 60, true);
			FP.screen.scale = 2;
			
			// Settings.
			Input.define("jump", Key.SPACE);
			Input.define("swap", Key.R, Key.TAB, Key.CONTROL, Key.V, Key.T, Key.F, Key.UP, Key.DOWN, Key.J);
			Input.define("flip", Key.D, Key.E, Key.ENTER, Key.S, Key.C, Key.SHIFT, Key.K);
			Input.define("menu", Key.ENTER);
			Input.define("pause", Key.ESCAPE, Key.P);
			
			// Debug console.
			if (DEBUG) {
				FP.console.enable();
			}
			
			FP.randomizeSeed();
			
			// Start game.
			FP.world = new IntroWorld();
			//FP.world = new MenuWorld();
			//FP.world = new GameWorld(1);
			//FP.world = new GameWorld(2);
			//FP.world = new GameWorld(3);
			//FP.world = new GameWorld(4);
			//FP.world = new GameWorld(5);
			//FP.world = new GameWorld(6);
			//FP.world = new JukeboxWorld();
		}
	}
}
