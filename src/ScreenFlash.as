package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ScreenFlash extends Entity {
		[Embed(source='../img/flash.png')]
		private const kSpriteFile:Class;
		
		private var image:Image;
		
		private var fadeRate:Number;
		
		public function reset(color:uint, alpha:Number, fadeRate:Number = 0.05):void {
			image = new Image(kSpriteFile);
			image.scaleX = 400;
			image.scaleY = 300;
			image.color = color;
			x = 0;
			y = 0;
			image.scrollX = 0;
			image.scrollY = 0;
			graphic = image;
			image.alpha = alpha;
			this.fadeRate = fadeRate;
			
			layer = -3000;
		}
		
		override public function update():void {
			image.alpha -= fadeRate;
			if (image.alpha <= 0) {
				world.recycle(this);
			}
		}
	}
}
