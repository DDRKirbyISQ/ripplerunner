package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import punk.fx.graphics.FXImage;
	import punk.fx.graphics.FXLayer;
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.AdjustFX;
	import net.flashpunk.graphics.Emitter;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GameWorld extends World {
		private static var instance:GameWorld;
		
		[Embed(source='../img/flash.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		private var backdrop1:Backdrop;
		private var backdrop1r:Backdrop;
		private var backdrop2:Backdrop;
		private var backdrop2r:Backdrop;
		private var backdrop3:Backdrop;
		private var backdrop3r:Backdrop;

		[Embed(source='../img/backdrop_1.png')]
		private static const kBackdrop1File:Class;
		[Embed(source='../img/backdrop_1r.png')]
		private static const kBackdrop1rFile:Class;
		[Embed(source='../img/backdrop_2.png')]
		private static const kBackdrop2File:Class;
		[Embed(source='../img/backdrop_2r.png')]
		private static const kBackdrop2rFile:Class;
		[Embed(source='../img/backdrop_3.png')]
		private static const kBackdrop3File:Class;
		[Embed(source='../img/backdrop_3r.png')]
		private static const kBackdrop3rFile:Class;

		[Embed(source='../img/backdrop_2_1.png')]
		private static const kBackdrop2_1File:Class;
		[Embed(source='../img/backdrop_2_1r.png')]
		private static const kBackdrop2_1rFile:Class;
		[Embed(source='../img/backdrop_2_2.png')]
		private static const kBackdrop2_2File:Class;
		[Embed(source='../img/backdrop_2_2r.png')]
		private static const kBackdrop2_2rFile:Class;
		[Embed(source='../img/backdrop_2_3.png')]
		private static const kBackdrop2_3File:Class;
		[Embed(source='../img/backdrop_2_3r.png')]
		private static const kBackdrop2_3rFile:Class;
		
		[Embed(source='../img/backdrop_3_1.png')]
		private static const kBackdrop3_1File:Class;
		[Embed(source='../img/backdrop_3_1r.png')]
		private static const kBackdrop3_1rFile:Class;
		[Embed(source='../img/backdrop_3_2.png')]
		private static const kBackdrop3_2File:Class;
		[Embed(source='../img/backdrop_3_2r.png')]
		private static const kBackdrop3_2rFile:Class;
		[Embed(source='../img/backdrop_3_3.png')]
		private static const kBackdrop3_3File:Class;
		[Embed(source='../img/backdrop_3_3r.png')]
		private static const kBackdrop3_3rFile:Class;

		[Embed(source='../img/backdrop_4_1.png')]
		private static const kBackdrop4_1File:Class;
		[Embed(source='../img/backdrop_4_1r.png')]
		private static const kBackdrop4_1rFile:Class;
		[Embed(source='../img/backdrop_4_2.png')]
		private static const kBackdrop4_2File:Class;
		[Embed(source='../img/backdrop_4_2r.png')]
		private static const kBackdrop4_2rFile:Class;
		[Embed(source='../img/backdrop_4_3.png')]
		private static const kBackdrop4_3File:Class;
		[Embed(source='../img/backdrop_4_3r.png')]
		private static const kBackdrop4_3rFile:Class;
		
		[Embed(source='../img/backdrop_5_1.png')]
		private static const kBackdrop5_1File:Class;
		[Embed(source='../img/backdrop_5_1r.png')]
		private static const kBackdrop5_1rFile:Class;
		[Embed(source='../img/backdrop_5_2.png')]
		private static const kBackdrop5_2File:Class;
		[Embed(source='../img/backdrop_5_2r.png')]
		private static const kBackdrop5_2rFile:Class;
		[Embed(source='../img/backdrop_5_3.png')]
		private static const kBackdrop5_3File:Class;
		[Embed(source='../img/backdrop_5_3r.png')]
		private static const kBackdrop5_3rFile:Class;
		
		[Embed(source='../img/backdrop_6_1.png')]
		private static const kBackdrop6_1File:Class;
		[Embed(source='../img/backdrop_6_1r.png')]
		private static const kBackdrop6_1rFile:Class;
		[Embed(source='../img/backdrop_6_2.png')]
		private static const kBackdrop6_2File:Class;
		[Embed(source='../img/backdrop_6_2r.png')]
		private static const kBackdrop6_2rFile:Class;
		[Embed(source='../img/backdrop_6_3.png')]
		private static const kBackdrop6_3File:Class;
		[Embed(source='../img/backdrop_6_3r.png')]
		private static const kBackdrop6_3rFile:Class;
		
		[Embed(source='../img/water.png')]
		private static const kWaterFile:Class;
		private var water:Backdrop = new Backdrop(kWaterFile, true, false);
		[Embed(source='../img/water2.png')]
		private static const kWater2File:Class;
		private var water2:Backdrop = new Backdrop(kWater2File, true, false);
		
		[Embed(source='../mus/stage1.mp3')]
		private static const kLevel1SongFile:Class;
		private var level1Song:Sfx = new Sfx(kLevel1SongFile);
		
		[Embed(source='../mus/stage2.mp3')]
		private static const kLevel2SongFile:Class;
		private var level2Song:Sfx = new Sfx(kLevel2SongFile);

		[Embed(source='../mus/stage3.mp3')]
		private static const kLevel3SongFile:Class;
		private var level3Song:Sfx = new Sfx(kLevel3SongFile);

		[Embed(source='../mus/stage4.mp3')]
		private static const kLevel4SongFile:Class;
		private var level4Song:Sfx = new Sfx(kLevel4SongFile);

		[Embed(source='../mus/stage5.mp3')]
		private static const kLevel5SongFile:Class;
		private var level5Song:Sfx = new Sfx(kLevel5SongFile);

		[Embed(source='../mus/stage6.mp3')]
		private static const kLevel6SongFile:Class;
		private var level6Song:Sfx = new Sfx(kLevel6SongFile);

		[Embed(source='../sfx/death.mp3')]
		private static const kDeathSfxFile:Class;
		private var deathSfx:Sfx = new Sfx(kDeathSfxFile);
		
		public var song:Sfx;
		
		public var level:int;
		
		public var adjustFx:AdjustFX = new AdjustFX();
		
		public var fxLayer:FXLayer;
		
		public var respawnTime:Number = 0.0;
		public var respawnY:Number = -40;

		private var pauseText:Text = new Text("Paused", 200, 140);
		private var pauseText2:Text = new Text("Press Escape to continue, or Enter to quit", 200, 160);
		
		private var pauseShadowEntity:Entity;
		
		[Embed(source='../sfx/switch_level_1.mp3')]
		private static const kSwitch1File:Class;
		private var switch1Sfx:Sfx = new Sfx(kSwitch1File);
		
		[Embed(source='../sfx/tutorial.mp3')]
		private static const kPauseSfxFile:Class;
		private var pauseSfx:Sfx = new Sfx(kPauseSfxFile);
		
		[Embed(source='../img/particle_small.png')]
		private const particleFile2:Class;
		// player particles
		public var emitter2:Emitter;
		
		[Embed(source='../img/particle_medium.png')]
		private const particleFile3:Class;
		// player swap particles
		public var emitter3:Emitter;

		public static const kCameraX:int = 80;
		
		public var deaths:int = 0;
		
		public var paused:Boolean = false;
		
		// Returns you the GameWorld.  Valid even in its constructor.
		public static function world():GameWorld {
			return instance as GameWorld;
		}
		
		public function GameWorld(level:int) {
			FP.screen.color = 0xFFFFFF;
			instance = this;
			
			var backdropFxLayer2 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer2.effects.add(new GlitchFX(2));
			addGraphic(backdropFxLayer2);
			
			var backdropFxLayer3 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer3.effects.add(new GlitchFX(3));
			addGraphic(backdropFxLayer3);
			
			var colorTransformFX:GlitchFX = new GlitchFX(5);
			fxLayer = new FXLayer(); // with no parameters will default to the size of the screen
			fxLayer.effects.add(colorTransformFX);
			addGraphic(fxLayer);

			
			if (level == 1) {
				song = level1Song;
				backdrop1 = new Backdrop(kBackdrop1File, true, false);
				backdrop1r = new Backdrop(kBackdrop1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop2File, true, false);
				backdrop2r = new Backdrop(kBackdrop2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop3File, true, false);
				backdrop3r = new Backdrop(kBackdrop3rFile, true, false);
			} else if (level == 2) {
				song = level2Song;
				backdrop1 = new Backdrop(kBackdrop2_1File, true, false);
				backdrop1r = new Backdrop(kBackdrop2_1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop2_2File, true, false);
				backdrop2r = new Backdrop(kBackdrop2_2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop2_3File, true, false);
				backdrop3r = new Backdrop(kBackdrop2_3rFile, true, false);
			} else if (level == 3) {
				song = level3Song;
				backdrop1 = new Backdrop(kBackdrop3_1File, true, false);
				backdrop1r = new Backdrop(kBackdrop3_1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop3_2File, true, false);
				backdrop2r = new Backdrop(kBackdrop3_2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop3_3File, true, false);
				backdrop3r = new Backdrop(kBackdrop3_3rFile, true, false);
			} else if (level == 4) {
				song = level4Song;
				backdrop1 = new Backdrop(kBackdrop4_1File, true, false);
				backdrop1r = new Backdrop(kBackdrop4_1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop4_2File, true, false);
				backdrop2r = new Backdrop(kBackdrop4_2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop4_3File, true, false);
				backdrop3r = new Backdrop(kBackdrop4_3rFile, true, false);
			} else if (level == 5) {
				song = level5Song;
				backdrop1 = new Backdrop(kBackdrop5_1File, true, false);
				backdrop1r = new Backdrop(kBackdrop5_1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop5_2File, true, false);
				backdrop2r = new Backdrop(kBackdrop5_2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop5_3File, true, false);
				backdrop3r = new Backdrop(kBackdrop5_3rFile, true, false);
			} else if (level == 6) {
				song = level6Song;
				backdrop1 = new Backdrop(kBackdrop6_1File, true, false);
				backdrop1r = new Backdrop(kBackdrop6_1rFile, true, false);
				backdrop2 = new Backdrop(kBackdrop6_2File, true, false);
				backdrop2r = new Backdrop(kBackdrop6_2rFile, true, false);
				backdrop3 = new Backdrop(kBackdrop6_3File, true, false);
				backdrop3r = new Backdrop(kBackdrop6_3rFile, true, false);
			}
			
			backdrop1.scrollX = 0.05;
			backdrop1r.scrollX = 0.05;
			backdrop1.y = -FP.halfHeight;
			addGraphic(backdrop1, 1000);
			var entity:Entity = new Entity(0, 0, backdrop1r);
			entity.layer = 1000;
			add(entity);
			
			backdrop2.scrollX = 0.15;
			backdrop2r.scrollX = 0.15;
			backdrop2.y = -FP.halfHeight;
			addGraphic(backdrop2, 1000);
			entity = new Entity(0, 0, backdrop2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			
			backdrop3.scrollX = 0.25;
			backdrop3r.scrollX = 0.25;
			backdrop3.y = -FP.halfHeight;
			addGraphic(backdrop3, 1000);
			entity = new Entity(0, 0, backdrop3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			
			this.level = level;

			add(new Player(0, -45, false));
			add(new Player(0, 45, true));

			Songs.SetY();
			
			addGraphic(water);
			addGraphic(water2);
			water2.scrollX = 0.8;
			water2.y = -FP.halfHeight;
			water.y = -FP.halfHeight;
			
			Songs.LoadLevel(level);
			
			song.play(1, 0, 0.0);
			//Player.player().y = -1000;
			//song.play(1, 0, 60 + 20.0);
			
			var fxScreen:FXImage = new FXImage();
			fxScreen.effects.add(adjustFx);
			addGraphic(fxScreen, -9000);
			
			// sync with camera & clear the screen before drawing fxImage
			fxScreen.onPreRender = function(img:FXImage):void {
				img.x = FP.camera.x;
				img.y = FP.camera.y;
				FP.screen.refresh();
			}
			
			adjustFx.hue = 0;
			
			fxLayer.active = false;
			
			emitter2 = new Emitter(particleFile2, 3, 3);
			emitter2.newType("regular", [0]);
			emitter2.setMotion("regular", 160, 0, 0, 40, 60, 60);
			emitter2.setAlpha("regular");
			emitter2.scrollX = 0;
			addGraphic(emitter2);

			emitter3 = new Emitter(particleFile3, 5, 5);
			emitter3.newType("regular", [0]);
			emitter3.setMotion("regular", 160, 0, 0, 360, 10, 60);
			emitter3.setAlpha("regular");
			emitter3.scrollX = 0;
			addGraphic(emitter3);
			
			var screenFlash:ScreenFlash = GameWorld.world().create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 1, 0.01);
			
			addGraphic(blackImage, -10000);
			blackImage.scrollX = 0;
			blackImage.scrollY = 0;
			blackImage.scaleX = 400;
			blackImage.scaleY = 300;
			blackImage.alpha = 0;
			
			pauseText.scrollX = 0;
			pauseText.scrollY = 0
			pauseText.centerOrigin();
			pauseText2.scrollX = 0;
			pauseText2.scrollY = 0
			pauseText2.scale = 0.5;
			pauseText2.centerOrigin();
			addGraphic(pauseText, -5000);
			addGraphic(pauseText2, -5000);
			pauseShadowEntity = new Entity(0, 0);
			ShadowText.AddShadowText(pauseText, pauseShadowEntity, 1);
			ShadowText.AddShadowText(pauseText2, pauseShadowEntity, 1);
			add(pauseShadowEntity);
			pauseText.visible = false;
			pauseText2.visible = false;
			pauseShadowEntity.visible = false;
		}
		
		private var finishing:Boolean = false;

		public var songPos:Number = 0;
		
		override public function update():void {
			
			songPos += FP.elapsed;
			if (songPos < 0 || Math.abs(songPos - song.position) > .06) {
				songPos = song.position;
				//trace(songPos - song.position);
			} else if (songPos < 0 || Math.abs(songPos - song.position) > .03) {
				songPos = (songPos + song.position) / 2;
				//trace(songPos - song.position);
				
				//deathSfx.play();
			}
			
			super.update();
			FP.camera.y = -FP.halfHeight;
			FP.camera.x = Player.player().x - kCameraX;
			
			if (Main.DEBUG) {
				if (Input.pressed(Key.RIGHT)) {
					song.play(1, 0, song.position + 5);
					Player.player().y = -1000;
					Player.reflection().y = 1000;
				}
				if (Input.pressed(Key.LEFT)) {
					song.play(1, 0, song.position - 5);
					Player.player().y = -1000;
					Player.reflection().y = 1000;
				}
			}
			
			if (Input.pressed("pause") && !finishing) {
				pauseText.visible = !pauseText.visible;
				pauseText2.visible = !pauseText2.visible;
				pauseShadowEntity.visible = !pauseShadowEntity.visible;
				paused = pauseText.visible;

				pauseSfx.play();
				
				if (pauseText.visible) {
					song.stop();
				} else {
					song.resume();
				}
			}
			
			if (Input.pressed(Key.ENTER) && paused && !finishing) {
				switch1Sfx.play();
				finishing = true;
			}
			
			if (song.position > Songs.FinishTime(level)) {
				finishing = true;
			}
			
			if (finishing) {
				if (blackImage.alpha >= 1.0) {
					// Finish.
					if (deaths == 0 && !paused) {
						if (level == 1) {
							MenuWorld.level1Medal = true;
						}
						if (level == 2) {
							MenuWorld.level2Medal = true;
						}
						if (level == 3) {
							MenuWorld.level3Medal = true;
						}
						if (level == 4) {
							MenuWorld.level4Medal = true;
						}
						if (level == 5) {
							MenuWorld.level5Medal = true;
						}
						if (level == 6) {
							MenuWorld.level6Medal = true;
						}
					}
					FP.world = new MenuWorld();
					return;
				}
				blackImage.alpha += 1 / 60.0;
			}
		}
		
		public function die():void {
			song.stop();
			song.play(1, 0, respawnTime);
			Player.player().isActive = true;
			Player.reflection().isActive = false;
			Player.player().y = -100;
			Player.player().yVel = 10;
			var screenFlash:ScreenFlash = GameWorld.world().create(ScreenFlash) as ScreenFlash;
			screenFlash.reset(0xFFFFFF, 0.75, 0.01);
			deathSfx.play();
			
			deaths++;
			
			if (level == 2 && respawnTime > 57.0 && respawnTime < 58.0) {
				adjustFx.hue = 0;
			}
		}
	}
}
