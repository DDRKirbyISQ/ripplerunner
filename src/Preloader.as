package
{
	import flash.display.*;
	import flash.text.*;
	import flash.events.*;
	import flash.utils.getDefinitionByName;
 
	[SWF(width = "800", height = "600")]
	public class Preloader extends Sprite
	{
		// Change these values
		private static const mustClick: Boolean = true;
		private static const mainClassName: String = "Main";
		
		private static const BG_COLOR:uint = 0x000000;
		private static const FG_COLOR:uint = 0xFFFFFF;
		
		[Embed(source = 'net/flashpunk/graphics/04B_03__.TTF', embedAsCFF="false", fontFamily = 'default')]
		private static const FONT:Class;
		
		[Embed(source='../img/frame1.png')]
		private static const frame1File:Class;
		[Embed(source='../img/frame2.png')]
		private static const frame2File:Class;
		[Embed(source='../img/frame3.png')]
		private static const frame3File:Class;
		[Embed(source='../img/frame4.png')]
		private static const frame4File:Class;
		[Embed(source='../img/frame5.png')]
		private static const frame5File:Class;
		
		private var frame1:Bitmap = new frame1File();
		private var frame2:Bitmap = new frame2File();
		private var frame3:Bitmap = new frame3File();
		private var frame4:Bitmap = new frame4File();
		private var frame5:Bitmap = new frame5File();
		
		// Ignore everything else
		
		
		
		private var progressBar: Shape;
		private var text: TextField;
		
		private var px:int;
		private var py:int;
		private var w:int;
		private var h:int;
		private var sw:int;
		private var sh:int;
		
		public function Preloader ()
		{
			sw = stage.stageWidth;
			sh = stage.stageHeight;
			
			w = stage.stageWidth * 0.8;
			h = 20;
			
			px = (sw - w) * 0.5;
			py = (sh - h) * 0.5;
			
			graphics.beginFill(BG_COLOR);
			graphics.drawRect(0, 0, sw, sh);
			graphics.endFill();
			
			graphics.beginFill(FG_COLOR);
			graphics.drawRect(px - 2, py - 2, w + 4, h + 4);
			graphics.endFill();
			
			progressBar = new Shape();
			
			addChild(progressBar);
			
			text = new TextField();
			
			text.textColor = FG_COLOR;
			text.selectable = false;
			text.mouseEnabled = false;
			text.defaultTextFormat = new TextFormat("default", 16);
			text.embedFonts = true;
			text.autoSize = "left";
			text.text = "0%";
			text.x = (sw - text.width) * 0.5;
			text.y = sh * 0.5 + h;
			
			addChild(text);
			
			addChild(frame1);
			addChild(frame2);
			addChild(frame3);
			addChild(frame4);
			addChild(frame5);
			
			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			if (mustClick) {
				stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
		}
 
		public function onEnterFrame (e:Event): void
		{
			if (hasLoaded())
			{
				graphics.clear();
				graphics.beginFill(BG_COLOR);
				graphics.drawRect(0, 0, sw, sh);
				graphics.endFill();
				
				frame1.visible = false;
				frame2.visible = false;
				frame3.visible = false;
				frame4.visible = false;
				frame5.visible = false;
				
				if (! mustClick) {
					startup();
				} else {
					text.scaleX = 2.0;
					text.scaleY = 2.0;
				
					text.text = "Click to start";
			
					text.y = (sh - text.height) * 0.5;
				}
			} else {
				var p:Number = (loaderInfo.bytesLoaded / loaderInfo.bytesTotal);
				
				progressBar.graphics.clear();
				progressBar.graphics.beginFill(BG_COLOR);
				progressBar.graphics.drawRect(px, py, p * w, h);
				progressBar.graphics.endFill();
				
				text.text = int(p * 100) + "%";
				
				frame1.x = p * w + 51;
				frame1.y = py - 51;
				frame2.x = p * w + 51;
				frame2.y = py - 51;
				frame3.x = p * w + 51;
				frame3.y = py - 51;
				frame4.x = p * w + 51;
				frame4.y = py - 51;
				frame5.x = p * w + 51;
				frame5.y = py - 51;
				
				frame1.visible = int(p * 100) % 8 == 0;
				frame2.visible = int(p * 100) % 8 == 1 || int(p * 100) % 8 == 7;
				frame3.visible = int(p * 100) % 8 == 2 || int(p * 100) % 8 == 6;
				frame4.visible = int(p * 100) % 8 == 3 || int(p * 100) % 8 == 5;
				frame5.visible = int(p * 100) % 8 == 4 || int(p * 100) % 8 == 4;
			}
			
			text.x = (sw - text.width) * 0.5;
		}
		
		private function onMouseDown(e:MouseEvent):void {
			if (hasLoaded())
			{
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				startup();
			}
		}
		
		private function hasLoaded (): Boolean {
			return (loaderInfo.bytesLoaded >= loaderInfo.bytesTotal);
		}
		
		private function startup (): void {
			stage.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			var mainClass:Class = getDefinitionByName(mainClassName) as Class;
			parent.addChild(new mainClass as DisplayObject);
			
			parent.removeChild(this);
		}
	}
}
