package  
{
	import net.flashpunk.Entity;
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class HueShift extends Entity
	{
		private var time:Number;
		
		private var triggered:Boolean = false;
		private var hue:Number;
		private var flash:Boolean;
		
		public function HueShift(time:Number, hue:Number, flash:Boolean)
		{
			this.time = time;
			this.hue = hue;
			this.flash = flash;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (GameWorld.world().song.position <= time - 0.5) {
				// reset for respawn
				
				// TODO: ACTUALLY RESET HUE CORRECTLY FOR RESPAWN
				// right now hue must be at a checkpoint.
				
				triggered = false;
			}

			if (GameWorld.world().song.position >= time && !triggered) {
				triggered = true;
				GameWorld.world().adjustFx.hue = hue;
				
				if (flash) {
					var screenFlash:ScreenFlash = GameWorld.world().create(ScreenFlash) as ScreenFlash;
					screenFlash.reset(0xFFFFFF, 0.5, 0.03);
				}
				return;
			}
		}
	}
}
