package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Ease;
	import punk.fx.graphics.FXImage;
	import punk.fx.graphics.FXLayer;
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.AdjustFX;
	import net.flashpunk.graphics.Emitter;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class IntroWorld extends World {
		[Embed(source='../img/flash.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		

		
		private static var instance:IntroWorld;
		
		private var startX:Number;
		private var startY:Number;
		
		[Embed(source='../mus/intro.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);
		
		private var text:Text = new Text("DDRKirby(ISQ) presents", FP.halfWidth, FP.halfHeight - 20);
		
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 30, 24);
		
		private var spritemap2:Spritemap = new Spritemap(kSpritemapFile, 30, 24);

		
		private var timer:int = -1;

		
		[Embed(source='../img/backdrop_1.png')]
		private static const kBackdrop1File:Class;
		private var backdrop1:Backdrop = new Backdrop(kBackdrop1File, true, false);
		[Embed(source='../img/backdrop_1r.png')]
		private static const kBackdrop1rFile:Class;
		private var backdrop1r:Backdrop = new Backdrop(kBackdrop1rFile, true, false);
		
		[Embed(source='../img/backdrop_2.png')]
		private static const kBackdrop2File:Class;
		private var backdrop2:Backdrop = new Backdrop(kBackdrop2File, true, false);
		[Embed(source='../img/backdrop_2r.png')]
		private static const kBackdrop2rFile:Class;
		private var backdrop2r:Backdrop = new Backdrop(kBackdrop2rFile, true, false);
		
		[Embed(source='../img/backdrop_3.png')]
		private static const kBackdrop3File:Class;
		private var backdrop3:Backdrop = new Backdrop(kBackdrop3File, true, false);
		[Embed(source='../img/backdrop_3r.png')]
		private static const kBackdrop3rFile:Class;
		private var backdrop3r:Backdrop = new Backdrop(kBackdrop3rFile, true, false);

		[Embed(source='../img/water.png')]
		private static const kWaterFile:Class;
		private var water:Backdrop = new Backdrop(kWaterFile, true, false);
		[Embed(source='../img/water2.png')]
		private static const kWater2File:Class;
		private var water2:Backdrop = new Backdrop(kWater2File, true, false);
		
		
		private static const kFadeRate:Number = 1.0 / 30.0;
		
		// Returns you the GameWorld.  Valid even in its constructor.
		public static function world():IntroWorld {
			return instance as IntroWorld;
		}
				public var fxLayer:FXLayer;

		public function IntroWorld() {
			instance = this;
			FP.screen.color = 0x000000;
			
			var backdropFxLayer2 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer2.effects.add(new GlitchFX(2));
			addGraphic(backdropFxLayer2);
			
			var backdropFxLayer3 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer3.effects.add(new GlitchFX(3));
			addGraphic(backdropFxLayer3);

			var colorTransformFX:GlitchFX = new GlitchFX(5);
			fxLayer = new FXLayer(); // with no parameters will default to the size of the screen
			fxLayer.effects.add(colorTransformFX);
			addGraphic(fxLayer);
			
			backdrop1.scrollX = 0.05;
			backdrop1r.scrollX = 0.05;
			backdrop1r.y = FP.halfHeight;
			addGraphic(backdrop1, 1000);
			var entity:Entity = new Entity(0, 0, backdrop1r);
			entity.layer = 1000;
			add(entity);
		
			backdrop2.scrollX = 0.15;
			backdrop2r.scrollX = 0.15;
			backdrop2r.y = FP.halfHeight;
			addGraphic(backdrop2, 1000);
			entity = new Entity(0, 0, backdrop2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			
			backdrop3.scrollX = 0.25;
			backdrop3r.scrollX = 0.25;
			backdrop3r.y = FP.halfHeight;
			addGraphic(backdrop3, 1000);
			entity = new Entity(0, 0, backdrop3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			
			addGraphic(water);
			addGraphic(water2);
			water2.scrollX = 0.8;
		
			blackImage.alpha = 1;
			blackImage.scaleX = 400;
			blackImage.scaleY = 300;
			blackImage.scrollX = 0;
			
			var blackEntity:Entity = new Entity(0, 0, blackImage);
			blackEntity.layer = -10000;
			add(blackEntity);
			
			
			add(new Platform(0, 800, 100, false, 10, true));
			add(new Platform(0, 800, 200, true, 10, true));
			
			spritemap.add("walk", [0, 1, 2, 3, 4, 3, 2, 1], 0.2);
			spritemap.play("walk");
			var spriteEntity:Entity = new Entity(0, 0, spritemap);
			spriteEntity.layer = -8000;
			add(spriteEntity);

			spritemap2.add("walk", [0, 1, 2, 3, 4, 3, 2, 1], 0.2);
			spritemap2.play("walk");
			spritemap2.scaleY = -1;
			var spriteEntity2:Entity = new Entity(0, 0, spritemap2);
			spriteEntity2.layer = -8000;
			add(spriteEntity2);
			
			text.originX = text.textWidth / 2;
			text.originY = text.textHeight / 2;
			text.scrollX = 0;
			var shadow:Entity = new Entity(0, 0);
			ShadowText.AddShadowText(text, shadow, 1);
			shadow.layer = -8500;
			add(shadow);
			var textEntity:Entity = new Entity(0, 0, text);
			textEntity.layer = -9000;
			add(textEntity);
			
			fxLayer.entities.add(spriteEntity2);
			
				blackImage.color = 0x000000;
			
		}
		
		override public function update():void {
			super.update();
			
			timer++;
			if (timer <= 45) {
				blackImage.alpha = 1.0 - timer / 45.0;
			}
			
			if (timer == 0) {
				sfx.play();
			}
			
			FP.camera.x++;
			
			spritemap.x = (timer) * 2.5 + 20;
			spritemap.y = 100;
			spritemap.originY = 29;
			spritemap2.x = (timer) * 2.5 + 20;
			spritemap2.y = 200;
			spritemap2.originY = 29;
			
			if (timer >= 135) {
				blackImage.color = 0xffffff;
				blackImage.alpha = (timer - 135) / 45.0;
			}
			
			if (timer == 210) {
				instance = null;
				FP.world = new MenuWorld();
			}
		}
	}
}