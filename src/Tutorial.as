package  
{
	import com.greensock.loading.data.ImageLoaderVars;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Tutorial extends Entity
	{
		[Embed(source='../sfx/tutorial.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		private var startT:Number;
		private var endT:Number;
		
		private var started:Boolean = false;
		
		private var image:Image;
		
		private const kFadeTime:Number = 0.5;
		
		public function Tutorial(startT:Number, endT:Number, image:Image)
		{
			this.startT = startT;
			this.endT = endT;
			
			this.image = image;
			addGraphic(image);
			image.scrollX = 0;
			image.scrollY = 0;
			image.alpha = 0;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (GameWorld.world().song.position <= startT - 0.5) {
				// reset for respawn
				started = false;
				image.alpha = 0.0;
			}

			if (GameWorld.world().song.position >= endT) {
				image.alpha -= 1.0 / kFadeTime / 60;
				return;
			}
			
			if (GameWorld.world().song.position >= startT) {
				if (!started) {
					started = true;
					sfx.play();
				}
				
				image.alpha += 1.0 / kFadeTime / 60;
			}
		}
	}

}