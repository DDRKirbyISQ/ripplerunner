package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Screen;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Checkpoint extends Entity
	{
		[Embed(source='../img/checkpoint_off.png')]
		private static const kOffFile:Class;
		private var offImage:Image = new Image(kOffFile);
		[Embed(source='../img/checkpoint_on.png')]
		private static const kOnFile:Class;
		private var onImage:Image = new Image(kOnFile);
		
		private var time:Number;
		
		private var triggered:Boolean = false;
		
		public function Checkpoint(time:Number, y:Number)
		{
			this.time = time;
			addGraphic(offImage);
			addGraphic(onImage);
			offImage.originX = 32;
			onImage.originX = 32;
			offImage.originY = 64 + 5;
			onImage.originY = 64 + 5;
			onImage.visible = false;
			this.y = y;
			this.x = Songs.GetXFromTime(time, GameWorld.world().level);
		}
		
		override public function update():void 
		{
			super.update();
			if (GameWorld.world().song.position >= time && !triggered && Player.player().isActive) {
				onImage.visible = true;
				offImage.visible = false;
				triggered = true;
				GameWorld.world().respawnTime = time;
				GameWorld.world().respawnY = y;
				
				// TODO: particle effect, flash, whatever.
				var particle:ParticleExplosion = GameWorld.world().create(ParticleExplosion) as ParticleExplosion;
				particle.init(x, y - 32);
				var screenFlash:ScreenFlash = GameWorld.world().create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 0.5, 0.01);
			}
		}
	}

}