package {
	import com.greensock.loading.data.ImageLoaderVars;
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Ease;
	import punk.fx.graphics.FXImage;
	import punk.fx.graphics.FXLayer;
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.AdjustFX;
	import net.flashpunk.graphics.Emitter;	

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MenuWorld extends World {
		private static const kMenuStartY:int = 170;
		private static const kMenuHeight:int = 18;
		
		
		public static var level1Medal:Boolean = false;
		public static var level2Medal:Boolean = false;
		public static var level3Medal:Boolean = false;
		public static var level4Medal:Boolean = false;
		public static var level5Medal:Boolean = false;
		public static var level6Medal:Boolean = false;

		[Embed(source='../mus/menu.mp3')]
		private static const kMusFile:Class;
		private static var mus:Sfx = new Sfx(kMusFile);

		[Embed(source='../img/cursor.png')]
		private static const kCursorFile:Class;
		private var cursor:Image = new Image(kCursorFile);

		[Embed(source='../img/medal.png')]
		private static const kMedalFile:Class;
		private var medal:Image = new Image(kMedalFile);

		[Embed(source='../img/flash.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../img/title_text.png')]
		private static const kTitleTextFile:Class;
		private var titleTextImage:Image = new Image(kTitleTextFile);

		[Embed(source='../sfx/flip.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/switch_level_1.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);
		
		
		[Embed(source='../img/controls.png')]
		private static const kControlsFile:Class;
		private var controls:Image = new Image(kControlsFile);

		[Embed(source='../img/backdrop_1.png')]
		private static const kBackdrop1File:Class;
		private var backdrop1:Backdrop = new Backdrop(kBackdrop1File, true, false);
		[Embed(source='../img/backdrop_1r.png')]
		private static const kBackdrop1rFile:Class;
		private var backdrop1r:Backdrop = new Backdrop(kBackdrop1rFile, true, false);
		
		[Embed(source='../img/backdrop_2.png')]
		private static const kBackdrop2File:Class;
		private var backdrop2:Backdrop = new Backdrop(kBackdrop2File, true, false);
		[Embed(source='../img/backdrop_2r.png')]
		private static const kBackdrop2rFile:Class;
		private var backdrop2r:Backdrop = new Backdrop(kBackdrop2rFile, true, false);
		
		[Embed(source='../img/backdrop_3.png')]
		private static const kBackdrop3File:Class;
		private var backdrop3:Backdrop = new Backdrop(kBackdrop3File, true, false);
		[Embed(source='../img/backdrop_3r.png')]
		private static const kBackdrop3rFile:Class;
		private var backdrop3r:Backdrop = new Backdrop(kBackdrop3rFile, true, false);

		[Embed(source='../img/water.png')]
		private static const kWaterFile:Class;
		private var water:Backdrop = new Backdrop(kWaterFile, true, false);
		[Embed(source='../img/water2.png')]
		private static const kWater2File:Class;
		private var water2:Backdrop = new Backdrop(kWater2File, true, false);
		
		
		

		private var texts:Vector.<Text> = new Vector.<Text>();
		private var shadowEntity:Entity;
		
		private var creditsText:Text;
		private var creditsText2:Text;
		private var creditsText3:Text;
		
		private static var selectedChoice:int = 0;

		private var fadeTimer:int = -1;

		private static const kFadeDuration:int = 60;
		
		private static var firstTime:Boolean = true;
		
		private var timer:int = 0;
				public var fxLayer:FXLayer;

		public function MenuWorld() {
			FP.screen.color = 0xFFFFFF;
			
			var backdropFxLayer2 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer2.effects.add(new GlitchFX(2));
			addGraphic(backdropFxLayer2);
			
			var backdropFxLayer3 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer3.effects.add(new GlitchFX(3));
			addGraphic(backdropFxLayer3);
			
			var colorTransformFX:GlitchFX = new GlitchFX(5);
			fxLayer = new FXLayer(); // with no parameters will default to the size of the screen
			fxLayer.effects.add(colorTransformFX);
			addGraphic(fxLayer);
			
			backdrop1.scrollX = 0.05;
			backdrop1r.scrollX = 0.05;
			backdrop1r.y = FP.halfHeight;
			addGraphic(backdrop1, 1000);
			var entity:Entity = new Entity(0, 0, backdrop1r);
			entity.layer = 1000;
			add(entity);
			
			backdrop2.scrollX = 0.15;
			backdrop2r.scrollX = 0.15;
			backdrop2r.y = FP.halfHeight;
			addGraphic(backdrop2, 1000);
			entity = new Entity(0, 0, backdrop2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			
			backdrop3.scrollX = 0.25;
			backdrop3r.scrollX = 0.25;
			backdrop3r.y = FP.halfHeight;
			addGraphic(backdrop3, 1000);
			entity = new Entity(0, 0, backdrop3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			
			addGraphic(water);
			addGraphic(water2);
			water2.scrollX = 0.8;
		
			
			titleTextImage.originX = titleTextImage.width / 2;
			var copy:Image = new Image(kTitleTextFile);
			copy.scaleY = -1;
			copy.y = 270;
			copy.x = 200;
			copy.originX = copy.width / 2;
			addGraphic(titleTextImage);
			var eee:Entity = new Entity(0, 0, copy);
			add(eee);
			titleTextImage.y = 30;
			titleTextImage.x = 200;
			fxLayer.entities.add(eee);
			titleTextImage.scrollX = 0;
			copy.scrollX = 0;
			
			shadowEntity = new Entity(0, 0);
			
			var i:int = 0;
			for each (var choice:String in ["Stage 1", "Stage 2", "Stage 3", "Stage 4", "Stage 5", "Stage 6", "Controls", "Jukebox"]) {
				var text:Text = new Text(choice, 200, kMenuStartY + i * kMenuHeight - 82);
				text.originX = text.textWidth / 2;
				text.originY = text.textHeight / 2;
				text.scrollX = 0;
				text.scale = 0.5;
				texts.push(text);
				i++;
				
				if (i == 1 && level1Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				if (i == 2 && level2Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				if (i == 3 && level3Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				if (i == 4 && level6Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				if (i == 5 && level5Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				if (i == 6 && level4Medal) {
					var image:Image = new Image(kMedalFile);
					image.x = text.x + 70;
					image.y = text.y;
					image.centerOrigin();
					image.scrollX = 0;
					addGraphic(image);
				}
				
				
				//if (firstTime) {
					ShadowText.AddShadowText(text, shadowEntity, 1);
					add(shadowEntity);
				//}
				addGraphic(text);
			}

			creditsText3 = new Text("", FP.halfWidth, FP.height - 70);
			creditsText3.originX = creditsText3.textWidth / 2;
			creditsText3.scrollX = 0;
				ShadowText.AddShadowText(creditsText3, shadowEntity, 1);
			addGraphic(creditsText3);
			creditsText = new Text("", FP.halfWidth, FP.height - 50);
			creditsText.originX = creditsText.textWidth / 2;
			creditsText.scrollX = 0;
				ShadowText.AddShadowText(creditsText, shadowEntity, 1);
			addGraphic(creditsText);
			creditsText2 = new Text("Version 1.23 / Programming, Art, and Music by DDRKirby(ISQ)", FP.halfWidth, FP.height - 15);
			creditsText2.scale = 0.5;
			creditsText2.originX = creditsText2.textWidth / 2;
			creditsText2.scrollX = 0;
				ShadowText.AddShadowText(creditsText2, shadowEntity, 1);
			addGraphic(creditsText2);
			
			firstTime = false;

			cursor.originX = cursor.width / 2;
			cursor.originY = cursor.height / 2;
			cursor.scale = 1;
			cursor.scrollX = 0;
			addGraphic(cursor);
			
			controls.scrollX = 0;
			controls.scrollY = 0;
			addGraphic(controls);
			controls.visible = false;

			blackImage.alpha = 1;
			blackImage.scaleX = 400;
			blackImage.scaleY = 300;
			blackImage.scrollX = 0;
			addGraphic(blackImage);
			
			mus.loop();
		}
		

		override public function update():void {
			super.update();
			
			FP.camera.x++;
			
			timer++;
			
			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
				mus.volume = fadeTimer / kFadeDuration;
				return;
			} else if (fadeTimer == 0) {
				if (selectedChoice == 0) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(1);
				} else if (selectedChoice == 1) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(2);
				} else if (selectedChoice == 2) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(3);
				} else if (selectedChoice == 3) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(6);
				} else if (selectedChoice == 4) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(5);
				} else if (selectedChoice == 5) {
					mus.stop();
					// reset timer.
					FP.world = new GameWorld(4);
				} else if (selectedChoice == 7) {
					mus.stop();
					FP.world = new JukeboxWorld();
				}
				return;
			}
			
			blackImage.alpha -= 2.0 / kFadeDuration;
			
			if (Input.pressed("menu")) {
				if (selectedChoice == 0) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 1) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 2) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 3) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 4) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 5) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				} else if (selectedChoice == 6) {
					confirmSfx.play();
					controls.visible = !controls.visible;
				} else if (selectedChoice == 7) {
					confirmSfx.play();
					fadeTimer = kFadeDuration;
				}
			}
			
			if (Input.pressed(Key.ESCAPE) && controls.visible) {
				controls.visible = !controls.visible;
				confirmSfx.play();
			}

			if (Input.pressed(Key.UP)) {
				cursorSfx.play();
				selectedChoice--;
			}
			if (Input.pressed(Key.DOWN)) {
				cursorSfx.play();
				selectedChoice++;
			}
			selectedChoice = (selectedChoice + texts.length) % texts.length;
			
			var selectedText:Text = texts[selectedChoice];
			cursor.x = 170;
			cursor.y = selectedText.y;
		}
	}
}
