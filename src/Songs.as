package {
	import com.bit101.components.NumericStepper;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Songs {
		public static function GetXFromTime(time:Number, level:int):Number {
			if (level == 1) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 2.0;
				
				var shift1T:Number = startTime + 144.0 / bps;
				
				if (time < shift1T) {
					return time * 200;
				} else {
					return shift1T * 200 + (time - shift1T) * 275;
				}
			}
			if (level == 2) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 100.0 / 60.0;
				
				var shift1T:Number = startTime + 48.0 / bps;
				var shift2T:Number = startTime + 80.0 / bps;
/*				var shift3T:Number = startTime + 108.0 / bps;
				var shift4T:Number = startTime + 109.0 / bps;
				var shift5T:Number = startTime + 110.0 / bps;
				var shift6T:Number = startTime + 111.0 / bps;*/
				var shift7T:Number = startTime + 112.0 / bps;
				
				if (time < shift1T) {
					return time * 200;
				} else if (time < shift7T) {
					return shift1T * 200 + (time - shift1T) * 300;
				} else {
					return shift1T * 200 + (shift7T - shift1T) * 300 + (time - shift7T) * 350;
				}
/*				} else if (time < shift4T) {
					return shift1T * 200 + (shift3T - shift1T) * 350 + (time - shift3T) * 100;
				} else if (time < shift5T) {
					return shift1T * 200 + (shift3T - shift1T) * 350 + (shift4T - shift3T) * 100 + (time - shift4T) * 200;
				} else if (time < shift6T) {
					return shift1T * 200 + (shift3T - shift1T) * 350 + (shift4T - shift3T) * 100 + (shift5T - shift4T) * 200 + (time - shift5T) * 275;
				} else if (time < shift7T) {
					return shift1T * 200 + (shift3T - shift1T) * 350 + (shift4T - shift3T) * 100 + (shift5T - shift4T) * 200 + (shift6T - shift5T) * 275 + (time - shift6T) * 350;
				} else {
					return shift1T * 200 + (shift3T - shift1T) * 350 + (shift4T - shift3T) * 100 + (shift5T - shift4T) * 200 + (shift6T - shift5T) * 275 + (shift7T - shift6T) * 350 + (time - shift7T) * 400;
				}*/
			}
			
			if (level == 3) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 140.0 / 60.0;
				
				var shift1T:Number = startTime + 64.0 / bps;
				var shift2T:Number = startTime + 144.0 / bps;
				var shift3T:Number = startTime + 160.0 / bps;
				
				if (time < shift1T) {
					return time * 350;
				} else if (time < shift2T ) {
					return shift1T * 350 + (time - shift1T) * 250;
				} else if (time < shift3T ) {
					return shift1T * 350 + (shift2T - shift1T) * 250 + (time - shift2T) * 350;
				} else {
					return shift1T * 350 + (shift2T - shift1T) * 250 + (shift3T - shift2T) * 350 + (time - shift3T) * 400;
				}
			}
			
			if (level == 4) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 160.0 / 60.0;
				
				return time * 400;
			}

			if (level == 5) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 90.0 / 60.0;
				
				var shift1T:Number = startTime + 76.0 / bps;
				var shift2T:Number = startTime + 79.0 / bps;

				if (time < shift1T) {
					return time * 300;
				} else if (time < shift2T ) {
					return shift1T * 300 + (time - shift1T) * 50;
				} else {
					return shift1T * 300 + (shift2T - shift1T) * 50 + (time - shift2T) * 350;
				}
				return time * 300;
			}
			
			if (level == 6) {
				var result:Number = 0;
				
				var startTime:Number = 0.0;
				var bps:Number = 160.0 / 60.0;
				
				return time * 300;
			}
			
			return time * 300;
		}
		
		public static function LoadLevel(level:int):void {
			if (level == 1) {
				LoadLevel1();
			}
			if (level == 2) {
				LoadLevel2();
			}
			if (level == 3) {
				LoadLevel3();
			}
			if (level == 4) {
				LoadLevel4();
			}
			if (level == 5) {
				LoadLevel5();
			}
			if (level == 6) {
				LoadLevel6();
			}
		}
		
		public static function FinishTime(level:int):Number {
			if (level == 1) {
				return 60 + 48;
			}
			if (level == 2) {
				return 60 + 60;
			}
			if (level == 3) {
				return 60 + 55;
			}
			if (level == 4) {
				return 120 + 17;
			}
			if (level == 5) {
				return 60 + 41;
			}
			if (level == 6) {
				return 60 + 41;
			}
			
			return 60;
		}
		
		public static function UpdateJumpGravity():void {
			var jumpVel:Number = -8;
			var gravity:Number = .6;
			
			if (GameWorld.world().level == 1) {
				jumpVel = -8.5;
				gravity = .6;
			} else if (GameWorld.world().level == 2) {
				var startTime:Number = 0.0;
				var bps:Number = 100.0 / 60.0;
				
				var shift1T:Number = startTime + 48.0 / bps;
				var shift2T:Number = startTime + 80.0 / bps;
				if (GameWorld.world().song.position < shift1T) {
					jumpVel = -7.5;
					gravity = .45;
				} else if (GameWorld.world().song.position < shift2T) {
					jumpVel = -4.25;
					gravity = .12;
				} else {
					jumpVel = -7.5;
					gravity = .45;
				}
			} else if (GameWorld.world().level == 3) {
				var bps:Number = 140.0 / 60.0;
				if (GameWorld.world().song.position < 154.0 / bps) {
					jumpVel = -9.5;
					gravity = .8;
				} else if (GameWorld.world().song.position < 161.0 / bps) {
					jumpVel = -3.5;
					gravity = .07;
				} else {
					jumpVel = -9.5;
					gravity = .8;
				}
			} else if (GameWorld.world().level == 4) {
				jumpVel = -9.5;
				gravity = .9;
			} else if (GameWorld.world().level == 5) {
				var bps:Number = 90.0 / 60.0;
				if (GameWorld.world().song.position < 75.5 / bps) {
					jumpVel = -8;
					gravity = .8;
				} else if (GameWorld.world().song.position < 79.0 / bps) {
					jumpVel = -1.1;
					gravity = .02;
				} else {
					jumpVel = -8;
					gravity = .8;
				}
			} else if (GameWorld.world().level == 6) {
				var bps:Number = 160.0 / 60.0;
				jumpVel = -9.5;
				gravity = .9;
			}
			
			Player.player().jumpVel = jumpVel;
			Player.player().gravity = gravity;
			Player.reflection().jumpVel = jumpVel;
			Player.reflection().gravity = gravity;
		}
		
		public static function SetY():void {
			var level:int = GameWorld.world().level;
			if (level == 1) {
				Player.player().y = -50;
				Player.reflection().y = 50;
			}
			if (level == 2) {
				Player.player().y = -50;
				Player.reflection().y = 50;
			}
			if (level == 3) {
				Player.player().y = -60;
				Player.reflection().y = 60;
			}
			if (level == 4) {
				Player.player().y = -60;
				Player.reflection().y = 60;
			}
			if (level == 5) {
				Player.player().y = -60;
				Player.reflection().y = 60;
			}
			if (level == 6) {
				Player.player().y = -60;
				Player.reflection().y = 60;
			}
		}
		
		[Embed(source='../img/tutorial1.png')]
		private static const kTutorial1File:Class;
		private static var tutorial1Image:Image = new Image(kTutorial1File);
		
		[Embed(source='../img/tutorial1b.png')]
		private static const kTutorial1bFile:Class;
		private static var tutorial1bImage:Image = new Image(kTutorial1bFile);
		
		[Embed(source='../img/tutorial2.png')]
		private static const kTutorial2File:Class;
		private static var tutorial2Image:Image = new Image(kTutorial2File);
		
		[Embed(source='../img/tutorial3.png')]
		private static const kTutorial3File:Class;
		private static var tutorial3Image:Image = new Image(kTutorial3File);
		
		[Embed(source='../img/tutorial4.png')]
		private static const kTutorial4File:Class;
		private static var tutorial4Image:Image = new Image(kTutorial4File);
		
		[Embed(source='../img/tutorial5.png')]
		private static const kTutorial5File:Class;
		private static var tutorial5Image:Image = new Image(kTutorial5File);
		
		[Embed(source='../img/tutorial6.png')]
		private static const kTutorial6File:Class;
		private static var tutorial6Image:Image = new Image(kTutorial6File);
		
		[Embed(source='../img/tutorial3.png')]
		private static const kTutorial7File:Class;
		private static var tutorial7Image:Image = new Image(kTutorial7File);
		
		[Embed(source='../img/tutorial8.png')]
		private static const kTutorial8File:Class;
		private static var tutorial8Image:Image = new Image(kTutorial8File);
		
		[Embed(source='../img/tutorial9.png')]
		private static const kTutorial9File:Class;
		private static var tutorial9Image:Image = new Image(kTutorial9File);
		
		[Embed(source='../img/tutorial10.png')]
		private static const kTutorial10File:Class;
		private static var tutorial10Image:Image = new Image(kTutorial10File);

		public static function LoadLevel1():void {
			var timingWindow:Number = 0.13;
			var level:int = 1;
			var startTime:Number = 0.0;
			var bps:Number = 2.0;
			
			GameWorld.world().respawnTime = startTime + 16.0 / bps;
			
			// Section 1
			addPlatform(startTime + -10.0 / bps, startTime + 36.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + -10.0 / bps, startTime + 36.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 37.0 / bps, startTime + 44.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 37.0 / bps, startTime + 44.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 45.0 / bps, startTime + 52.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 45.0 / bps, startTime + 52.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 53.0 / bps, startTime + 60.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 53.0 / bps, startTime + 60.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 61.0 / bps, startTime + 62.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 61.0 / bps, startTime + 62.0 / bps, 45, timingWindow, level);
			addPlatform(startTime + 63.0 / bps, startTime + 80.0 / bps, -40, timingWindow, level, true);
			addPlatform(startTime + 63.0 / bps, startTime + 80.0 / bps, 40, timingWindow, level, true);
			GameWorld.world().add(new Checkpoint(startTime + 64.0 / bps, -40));
			
			// Section 2
			addPlatform(startTime + 80.0 / bps, startTime + 100.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 80.0 / bps, startTime + 108.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 108.0 / bps, startTime + 116.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 116.0 / bps, startTime + 120.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 120.0 / bps, startTime + 124.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 124.0 / bps, startTime + 126.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 126.0 / bps, startTime + 128.0 / bps, -40, timingWindow, level, true);
			
			// Section 3
			GameWorld.world().add(new Checkpoint(startTime + 144.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 144.0 / bps, -60, true));
			addPlatform(startTime + 128.0 / bps, startTime + 146.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 147.0 / bps, startTime + 150.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 151.0 / bps, startTime + 152.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 152.0 / bps, startTime + 156.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 156.0 / bps, startTime + 158.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 158.0 / bps, startTime + 160.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 161.0 / bps, startTime + 164.0 / bps, 45, timingWindow, level);
			addPlatform(startTime + 165.0 / bps, startTime + 168.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 168.0 / bps, startTime + 172.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 173.0 / bps, startTime + 174.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 175.0 / bps, startTime + 176.0 / bps, -45, timingWindow, level, true);
			
			// Section 4
			GameWorld.world().add(new Checkpoint(startTime + 176.0 / bps, -45));
			GameWorld.world().add(new HueShift(startTime + 176.0 / bps, 60, true));
			addPlatform(startTime + 176.0 / bps, startTime + 184.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 184.0 / bps, startTime + 186.0 / bps, 45, timingWindow, level);
			addPlatform(startTime + 186.0 / bps, startTime + 188.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 189.0 / bps, startTime + 190.0 / bps, -50, timingWindow, level);
			addPlatform(startTime + 191.0 / bps, startTime + 196.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 196.0 / bps, startTime + 197.0 / bps, 45, timingWindow, level);
			addPlatform(startTime + 197.0 / bps, startTime + 204.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 205.0 / bps, startTime + 206.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 207.0 / bps, startTime + 208.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 209.0 / bps, startTime + 230.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 209.0 / bps, startTime + 230.0 / bps, 45, timingWindow, level);
			
			// Tutorials.
			GameWorld.world().add(new Tutorial(startTime + 2.0 / bps, startTime + 7.0 / bps, tutorial1Image));
			GameWorld.world().add(new Tutorial(startTime + 8.0 / bps, startTime + 15.0 / bps, tutorial1bImage));
			GameWorld.world().add(new Tutorial(startTime + 16.0 / bps, startTime + 23.0 / bps, tutorial2Image));
			GameWorld.world().add(new Tutorial(startTime + 24.0 / bps, startTime + 31.0 / bps, tutorial3Image));
			GameWorld.world().add(new Tutorial(startTime + 64.0 / bps, startTime + 71.0 / bps, tutorial4Image));
			GameWorld.world().add(new Tutorial(startTime + 72.0 / bps, startTime + 79.0 / bps, tutorial5Image));
			GameWorld.world().add(new Tutorial(startTime + 80.0 / bps, startTime + 91.0 / bps, tutorial6Image));
			GameWorld.world().add(new Tutorial(startTime + 92.0 / bps, startTime + 95.0 / bps, tutorial7Image));
			GameWorld.world().add(new Tutorial(startTime + 128.0 / bps, startTime + 140.0 / bps, tutorial8Image));
		}
		
		public static function LoadLevel2():void {
			var timingWindow:Number = 0.11;
			var timingWindowE:Number = 0.15;
			var level:int = 2;
			var startTime:Number = 0.0;
			var bps:Number = 100.0 / 60.0;
			
			GameWorld.world().respawnTime = startTime + 4.0 / bps;
			
			// Section 1
			addPlatform(startTime + -10.0 / bps, startTime + 16.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 17.0 / bps, startTime + 24.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 25.0 / bps, startTime + 33.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 34.0 / bps, startTime + 35.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 36.0 / bps, startTime + 37.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 38.0 / bps, startTime + 39.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 40.0 / bps, startTime + 41.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 42.0 / bps, startTime + 43.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 44.0 / bps, startTime + 45.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 46.0 / bps, startTime + 47.0 / bps, 40, timingWindow, level);
			GameWorld.world().add(new Checkpoint(startTime + 48.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 48.0 / bps, 150, true));
			
			// Section 2
			addPlatform(startTime + 48.0 / bps, startTime + 56.0 / bps, -40, timingWindowE, level);
			addPlatform(startTime + 58.0 / bps, startTime + 60.0 / bps, -40, timingWindowE, level);
			addPlatform(startTime + 62.0 / bps, startTime + 64.0 / bps, -40, timingWindowE, level);
			addPlatform(startTime + 66.0 / bps, startTime + 72.0 / bps, -40, timingWindowE, level);
			addPlatform(startTime + 74.0 / bps, startTime + 76.0 / bps, -40, timingWindowE, level);
			addPlatform(startTime + 78.0 / bps, startTime + 80.0 / bps, -40, timingWindowE, level);
			GameWorld.world().add(new HueShift(startTime + 80.0 / bps, 180, true));
			
			// Section 3
			addPlatform(startTime + 80.0 / bps, startTime + 81.5 / bps, 40, timingWindow, level);
			addPlatform(startTime + 81.5 / bps, startTime + 84.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 84.0 / bps, startTime + 85.5 / bps, 40, timingWindow, level);
			addPlatform(startTime + 85.5 / bps, startTime + 88.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 88.0 / bps, startTime + 89.5 / bps, 40, timingWindow, level);
			addPlatform(startTime + 89.5 / bps, startTime + 92.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 92.0 / bps, startTime + 93.5 / bps, 40, timingWindow, level);
			addPlatform(startTime + 93.5 / bps, startTime + 96.0 / bps, -40, timingWindow, level, true);
			GameWorld.world().add(new Checkpoint(startTime + 96.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 96.0 / bps, 0, true));
			
			// Section 3.5
			addPlatform(startTime + 96.0 / bps, startTime + 101.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 102.0 / bps, startTime + 103.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 104.0 / bps, startTime + 105.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 106.0 / bps, startTime + 107.0 / bps, -35, timingWindow, level);
			addPlatform(startTime + 108.0 / bps, startTime + 109.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 110.0 / bps, startTime + 113.0 / bps, -40, timingWindow, level);

			// Section 4
			GameWorld.world().add(new Checkpoint(startTime + 110.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 112.0 / bps, -90, true));
			addPlatform(startTime + 114.0 / bps, startTime + 115.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 116.0 / bps, startTime + 117.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 118.0 / bps, startTime + 119.0 / bps, 40, timingWindow, level);
			addPlatform(startTime + 120.0 / bps, startTime + 121.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 122.0 / bps, startTime + 123.0 / bps, -50, timingWindow, level);
			addPlatform(startTime + 124.0 / bps, startTime + 125.0 / bps, 50, timingWindow, level);
			addPlatform(startTime + 126.0 / bps, startTime + 127.0 / bps, 60, timingWindow, level);
			GameWorld.world().add(new Checkpoint(startTime + 128.0 / bps, -60));
			
			// Section 5
			addPlatform(startTime + 128.0 / bps, startTime + 129.5 / bps, -60, timingWindow, level);
			addPlatform(startTime + 130.0 / bps, startTime + 132.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 132.0 / bps, startTime + 134.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 134.0 / bps, startTime + 136.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 136.0 / bps, startTime + 141.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 142.0 / bps, startTime + 143.0 / bps, 30, timingWindow, level);
			
			addPlatform(startTime + 144.0 / bps, startTime + 146.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 146.0 / bps, startTime + 148.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 148.0 / bps, startTime + 150.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 150.0 / bps, startTime + 152.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 152.0 / bps, startTime + 156.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 156.0 / bps, startTime + 157.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 158.0 / bps, startTime + 159.0 / bps, 30, timingWindow, level);
			
			// Section 6
			GameWorld.world().add(new Checkpoint(startTime + 160.0 / bps, -30));
			addPlatform(startTime + 160.0 / bps, startTime + 163.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 164.0 / bps, startTime + 166.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 167.0 / bps, startTime + 167.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 168.0 / bps, startTime + 169.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 170.0 / bps, startTime + 171.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 172.0 / bps, startTime + 174.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 175.0 / bps, startTime + 175.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 176.0 / bps, startTime + 177.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 178.0 / bps, startTime + 179.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 180.0 / bps, startTime + 182.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 183.0 / bps, startTime + 183.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 184.0 / bps, startTime + 185.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 186.0 / bps, startTime + 187.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 188.0 / bps, startTime + 190.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 191.0 / bps, startTime + 191.0 / bps, 30, timingWindow, level);
			addPlatform(startTime + 192.0 / bps, startTime + 210.0 / bps, -30, timingWindow, level);
			addPlatform(startTime + 192.0 / bps, startTime + 210.0 / bps, 30, timingWindow, level);
			
			// Tutorials.
			GameWorld.world().add(new Tutorial(startTime + 4.0 / bps, startTime + 11.0 / bps, tutorial9Image));
			GameWorld.world().add(new Tutorial(startTime + 12.0 / bps, startTime + 15.0 / bps, tutorial3Image));
		}
		
		public static function LoadLevel3():void {
			var timingWindow:Number = 0.11;
			var level:int = 3;
			var startTime:Number = 0.0;
			var bps:Number = 140.0 / 60.0;
			
			GameWorld.world().respawnTime = startTime + 2.0 / bps;
			
			// Section 1
			addPlatform(startTime + -10.0 / bps, startTime + 20.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 20.0 / bps, startTime + 24.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 24.0 / bps, startTime + 28.0 / bps, -50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 28.0 / bps, startTime + 32.0 / bps, -50, timingWindow, level, false, true, false, false);

			addPlatform(startTime + -10.0 / bps, startTime + 20.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 20.0 / bps, startTime + 24.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 24.0 / bps, startTime + 28.0 / bps, 50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 28.0 / bps, startTime + 32.0 / bps, 50, timingWindow, level, false, true, false, false);

			// Section 2
			GameWorld.world().add(new Checkpoint(startTime + 32.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 32.0 / bps, 30, true));
			addPlatform(startTime + 32.0 / bps, startTime + 36.0 / bps, -50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 36.0 / bps, startTime + 40.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 40.0 / bps, startTime + 44.0 / bps, -50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 44.0 / bps, startTime + 48.0 / bps, -50, timingWindow, level, false, true, false, false);

			addPlatform(startTime + 32.0 / bps, startTime + 36.0 / bps, 50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 36.0 / bps, startTime + 40.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 40.0 / bps, startTime + 44.0 / bps, 50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 44.0 / bps, startTime + 48.0 / bps, 50, timingWindow, level, false, true, false, false);
			
			addPlatform(startTime + 48.0 / bps, startTime + 49.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 50.0 / bps, startTime + 51.0 / bps, -40, timingWindow, level);
			addPlatform(startTime + 52.0 / bps, startTime + 52.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 52.0 / bps, startTime + 56.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 56.0 / bps, startTime + 57.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 58.0 / bps, startTime + 59.0 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 60.0 / bps, startTime + 60.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 60.0 / bps, startTime + 64.0 / bps, -50, timingWindow, level, false, true, false, false);

			// Section 3
			GameWorld.world().add(new Checkpoint(startTime + 64.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 64.0 / bps, 60, true));
			addPlatform(startTime + 64.0 / bps, startTime + 73.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 74.0 / bps, startTime + 75.0 / bps, -55, timingWindow, level);
			addPlatform(startTime + 76.0 / bps, startTime + 77.0 / bps, 50, timingWindow, level);
			addPlatform(startTime + 78.0 / bps, startTime + 79.0 / bps, 55, timingWindow, level);
			addPlatform(startTime + 80.0 / bps, startTime + 82.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 82.0 / bps, startTime + 84.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 84.0 / bps, startTime + 86.0 / bps, -50, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 86.0 / bps, startTime + 88.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 88.0 / bps, startTime + 89.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 90.0 / bps, startTime + 91.0 / bps, -45, timingWindow, level);
			addPlatform(startTime + 92.0 / bps, startTime + 93.0 / bps, 45, timingWindow, level);
			addPlatform(startTime + 94.0 / bps, startTime + 95.0 / bps, 50, timingWindow, level);

			// Section 4
			addPlatform(startTime + 96.0 / bps, startTime + 98.0 / bps, -50, timingWindow, level);
			addPlatform(startTime + 98.0 / bps, startTime + 102.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 102.0 / bps, startTime + 104.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 104.0 / bps, startTime + 106.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 106.0 / bps, startTime + 110.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 110.0 / bps, startTime + 112.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 112.0 / bps, startTime + 114.0 / bps, -50, timingWindow, level, false, false, false, false);

			addPlatform(startTime + 114.0 / bps, startTime + 116.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 117.0 / bps, startTime + 118.0 / bps, 60, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 118.0 / bps, startTime + 120.0 / bps, 60, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 120.0 / bps, startTime + 122.0 / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 122.0 / bps, startTime + 124.0 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 125.0 / bps, startTime + 126.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 126.0 / bps, startTime + 128.0 / bps, -40, timingWindow, level, false, true, false, false);
			
			GameWorld.world().add(new Checkpoint(startTime + 128.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 128.0 / bps, 90, true));
			
			// Section 5
			addPlatform(startTime + 128.0 / bps, startTime + 136.0 / bps, -40, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 136.0 / bps, startTime + 144.0 / bps, -40, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 144.0 / bps, startTime + 146.0 / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 146.0 / bps, startTime + 148.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 148.0 / bps, startTime + 150.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 150.0 / bps, startTime + 152.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 152.0 / bps, startTime + (156.0 + 0.04) / bps, -40, timingWindow, level, false, false, true, true);

			// Section 6
			GameWorld.world().add(new Checkpoint(startTime + 160.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 160.0 / bps, 120, true));
			addPlatform(startTime + (160.0 - 0.04) / bps, startTime + 162.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 162.0 / bps, startTime + 166.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 166.0 / bps, startTime + 168.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 169.0 / bps, startTime + 170.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 171.0 / bps, startTime + 172.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 172.0 / bps, startTime + 176.0 / bps, -40, timingWindow, level, false, true, false, false);

			GameWorld.world().add(new Checkpoint(startTime + 176.0 / bps, -40));
			addPlatform(startTime + 176.0 / bps, startTime + 180.0 / bps, -40, timingWindow, level, false, false, false, false);
			addPlatform(startTime + 180.0 / bps, startTime + 182.0 / bps, -40, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 182.0 / bps, startTime + 183.0 / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 183.0 / bps, startTime + 184.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 184.0 / bps, startTime + 186.0 / bps, 40, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 186.0 / bps, startTime + 187.0 / bps, 40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 187.0 / bps, startTime + 194.0 / bps, -40, timingWindow, level, false, false, true, true);

			// Section 6
			GameWorld.world().add(new Checkpoint(startTime + 192.0 / bps, -40));
			GameWorld.world().add(new HueShift(startTime + 192.0 / bps, 150, true));
			addPlatform(startTime + 195.0 / bps, startTime + 198.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 199.0 / bps, startTime + 200.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 200.0 / bps, startTime + 208.0 / bps, -40, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 208.0 / bps, startTime + 209.0 / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 210.0 / bps, startTime + 211.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 212.0 / bps, startTime + 213.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 214.0 / bps, startTime + 215.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 216.0 / bps, startTime + 216.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 216.0 / bps, startTime + 224.0 / bps, -40, timingWindow, level, false, true, false, false);
			
			addPlatform(startTime + 224.0 / bps, startTime + 226.0 / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 226.0 / bps, startTime + 234.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 234.0 / bps, startTime + 242.0 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 242.0 / bps, startTime + 246.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 247.0 / bps, startTime + 250.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 250.0 / bps, startTime + 254.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 255.0 / bps, startTime + 280.0 / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 255.0 / bps, startTime + 280.0 / bps, 30, timingWindow, level, false, false, true, true);
			
			
			// Tutorials.
			GameWorld.world().add(new Tutorial(startTime + 2.0 / bps, startTime + 11.0 / bps, tutorial10Image));
			GameWorld.world().add(new Tutorial(startTime + 12.0 / bps, startTime + 15.0 / bps, tutorial3Image));
		}
		
		public static function LoadLevel4():void {
			var timingWindow:Number = 0.09;
			var level:int = 4;
			var startTime:Number = 0.0;
			var bps:Number = 160.0 / 60.0;
			
			GameWorld.world().respawnTime = startTime + 4.0 / bps;
			
			// Section 1
			addPlatform(startTime + -10.0 / bps, startTime + 21.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 22.0 / bps, startTime + 23.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 24.0 / bps, startTime + 25.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 26.0 / bps, startTime + 27.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 28.0 / bps, startTime + 29.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 30.0 / bps, startTime + 31.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 32.0 / bps, startTime + 32.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 32.0 / bps, startTime + 36.0 / bps, -50, timingWindow, level, false, true, false, false);

			// Section 2
			GameWorld.world().add(new Checkpoint(startTime + 36.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 36.0 / bps, 30, true));
			addPlatform(startTime + 36.0 / bps, startTime + 44.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 44.0 / bps, startTime + 45.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 45.5 / bps, startTime + 48.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 49.0 / bps, startTime + 50.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 51.0 / bps, startTime + 52.0 / bps, 45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 52.0 / bps, startTime + 54.0 / bps, 45, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 54.0 / bps, startTime + 54.0 / bps, 45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 55.0 / bps, startTime + 55.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 56.0 / bps, startTime + 56.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 56.0 / bps, startTime + 58.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 58.0 / bps, startTime + 59.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 60.0 / bps, startTime + 60.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 60.0 / bps, startTime + 61.5 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 61.5 / bps, startTime + 64.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 65.0 / bps, startTime + 66.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 67.0 / bps, startTime + 67.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 67.0 / bps, startTime + 68.0 / bps, -50, timingWindow, level, false, true, false, false);

			// Section 3
			GameWorld.world().add(new Checkpoint(startTime + 68.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 68.0 / bps, 60, true));
			addPlatform(startTime + 68.0 / bps, startTime + 76.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 76.0 / bps, startTime + 77.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 77.5 / bps, startTime + 80.0 / bps, -50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 81.0 / bps, startTime + 82.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 83.0 / bps, startTime + 84.0 / bps, 60, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 84.0 / bps, startTime + 86.0 / bps, 60, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 86.0 / bps, startTime + 86.0 / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 87.0 / bps, startTime + 87.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 88.0 / bps, startTime + 88.0 / bps, 50, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 88.0 / bps, startTime + 90.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 90.0 / bps, startTime + 90.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 91.0 / bps, startTime + 91.0 / bps, 45, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 92.0 / bps, startTime + 92.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 92.0 / bps, startTime + 93.5 / bps, -40, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 93.5 / bps, startTime + 96.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 97.0 / bps, startTime + 98.0 / bps, 45, timingWindow, level, false, false, true, true);

			// Section 4
			GameWorld.world().add(new Checkpoint(startTime + 100.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 100.0 / bps, 90, true));
			addPlatform(startTime + 99.0 / bps, startTime + 99.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 99.0 / bps, startTime + 100.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 100.0 / bps, startTime + 112.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 113.0 / bps, startTime + 113.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 114.0 / bps, startTime + 114.0 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 115.0 / bps, startTime + 115.0 / bps, -65, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 116.0 / bps, startTime + 116.0 / bps, -70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 116.0 / bps, startTime + 124.0 / bps, 70, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 124.0 / bps, startTime + 125.5 / bps, -70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 125.5 / bps, startTime + 127.0 / bps, 70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 128.0 / bps, startTime + 129.0 / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 129.0 / bps, startTime + 132.0 / bps, -60, timingWindow, level, false, true, false, false);

			addPlatform(startTime + 132.0 / bps, startTime + 132.0 / bps, -60, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 132.0 / bps, startTime + 133.5 / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 133.5 / bps, startTime + 135.0 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 135.0 / bps, startTime + 139.0 / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 140.0 / bps, startTime + 140.0 / bps, 55, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 140.0 / bps, startTime + 141.5 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 141.5 / bps, startTime + 143.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 143.0 / bps, startTime + 144.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 145.0 / bps, startTime + 145.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 146.0 / bps, startTime + 146.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 147.0 / bps, startTime + 147.0 / bps, -55, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 148.0 / bps, startTime + 148.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 149.0 / bps, startTime + 156.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 157.0 / bps, startTime + 172.0 / bps, -50, timingWindow, level, false, false, true, false);
			
			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 164.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 164.0 / bps, -60, true));
			addPlatform(startTime + 172.0 / bps, startTime + 174.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 174.0 / bps, startTime + 174.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 175.0 / bps, startTime + 175.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 176.0 / bps, startTime + 176.0 / bps, -50, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 176.0 / bps, startTime + 178.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 178.0 / bps, startTime + 178.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 179.0 / bps, startTime + 179.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 180.0 / bps, startTime + 180.0 / bps, -50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 180.0 / bps, startTime + 182.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 182.0 / bps, startTime + 183.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 184.0 / bps, startTime + 184.0 / bps, -50, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 184.0 / bps, startTime + 186.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 186.0 / bps, startTime + 186.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 186.0 / bps, startTime + 187.0 / bps, 50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 188.0 / bps, startTime + 188.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 188.0 / bps, startTime + 190.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 190.0 / bps, startTime + 191.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 192.0 / bps, startTime + 192.0 / bps, 50, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 192.0 / bps, startTime + 194.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 194.0 / bps, startTime + 194.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 194.0 / bps, startTime + 195.0 / bps, -50, timingWindow, level, false, false, true, true);

			// Section 6
			addPlatform(startTime + 196.0 / bps, startTime + 197.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 198.0 / bps, startTime + 199.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 200.0 / bps, startTime + 201.0 / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 202.0 / bps, startTime + 202.0 / bps, -45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 202.0 / bps, startTime + 204.0 / bps, -45, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 204.0 / bps, startTime + 205.0 / bps, -45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 206.0 / bps, startTime + 207.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 208.0 / bps, startTime + 208.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 208.0 / bps, startTime + 210.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 210.0 / bps, startTime + 210.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 211.0 / bps, startTime + 211.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 212.0 / bps, startTime + 212.0 / bps, 60, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 212.0 / bps, startTime + 213.5 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 214.5 / bps, startTime + 215.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 216.0 / bps, startTime + 216.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 216.0 / bps, startTime + 218.0 / bps, 55, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 218.0 / bps, startTime + 218.0 / bps, 55, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 219.0 / bps, startTime + 219.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 220.0 / bps, startTime + 220.0 / bps, 45, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 220.0 / bps, startTime + 221.0 / bps, -45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 221.0 / bps, startTime + 222.0 / bps, -45, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 222.0 / bps, startTime + 222.0 / bps, -45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 223.0 / bps, startTime + 223.0 / bps, -50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 224.0 / bps, startTime + 224.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 224.0 / bps, startTime + 226.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 226.0 / bps, startTime + 226.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 227.0 / bps, startTime + 227.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 228.0 / bps, startTime + 228.0 / bps, 50, timingWindow, level, false, false, true, true);

			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 228.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 228.0 / bps, -90, true));
			addPlatform(startTime + 228.0 / bps, startTime + 240.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 241.0 / bps, startTime + 241.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 242.5 / bps, startTime + 243.0 / bps, -50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 243.0 / bps, startTime + 245.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 246.0 / bps, startTime + 246.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 246.0 / bps, startTime + 247.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 247.0 / bps, startTime + 247.0 / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 248.0 / bps, startTime + 248.0 / bps, 50, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 248.0 / bps, startTime + 249.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 249.0 / bps, startTime + 250.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 250.0 / bps, startTime + 250.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 251.0 / bps, startTime + 251.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 252.0 / bps, startTime + 252.0 / bps, -50, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 252.0 / bps, startTime + 256.0 / bps, -50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 256.0 / bps, startTime + 256.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 257.0 / bps, startTime + 257.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 258.0 / bps, startTime + 258.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 259.0 / bps, startTime + 259.0 / bps, -55, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 260.0 / bps, startTime + 261.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 261.0 / bps, startTime + 263.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 263.0 / bps, startTime + 265.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 266.0 / bps, startTime + 266.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 266.0 / bps, startTime + 268.0 / bps, 55, timingWindow, level, false, true, false, false);

			addPlatform(startTime + 268.0 / bps, startTime + 268.0 / bps, 55, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 268.0 / bps, startTime + 269.5 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 269.5 / bps, startTime + 271.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 271.0 / bps, startTime + 272.0 / bps, 55, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 272.0 / bps, startTime + 272.0 / bps, 55, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 272.0 / bps, startTime + 273.5 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 273.5 / bps, startTime + 275.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 275.0 / bps, startTime + 276.0 / bps, 55, timingWindow, level, false, true, false, false);

			addPlatform(startTime + 276.0 / bps, startTime + 276.0 / bps, 55, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 277.0 / bps, startTime + 277.5 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 277.5 / bps, startTime + 279.0 / bps, -55, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 279.0 / bps, startTime + 280.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 281.0 / bps, startTime + 281.0 / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 282.0 / bps, startTime + 282.0 / bps, 65, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 283.0 / bps, startTime + 283.0 / bps, 70, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 284.0 / bps, startTime + 284.0 / bps, 75, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 284.0 / bps, startTime + 292.0 / bps, 75, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 292.0 / bps, startTime + 292.0 / bps, 75, timingWindow, level, false, false, false, true);


			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 292.0 / bps, -75));
			GameWorld.world().add(new HueShift(startTime + 292.0 / bps, 150, true));
			addPlatform(startTime + 292.0 / bps, startTime + 294.0 / bps, -75, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 295.0 / bps, startTime + 295.5 / bps, -70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 295.5 / bps, startTime + 297.0 / bps, 70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 297.0 / bps, startTime + 298.0 / bps, -70, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 299.0 / bps, startTime + 300.0 / bps, -65, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 300.0 / bps, startTime + 302.0 / bps, 65, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 303.0 / bps, startTime + 303.5 / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 303.5 / bps, startTime + 305.0 / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 305.0 / bps, startTime + 306.0 / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 307.0 / bps, startTime + 307.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 307.0 / bps, startTime + 308.0 / bps, 55, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 308.0 / bps, startTime + 308.0 / bps, 55, timingWindow, level, false, false, false, true);

			addPlatform(startTime + 308.0 / bps, startTime + 310.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 311.0 / bps, startTime + 311.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 311.5 / bps, startTime + 313.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 313.0 / bps, startTime + 314.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 315.0 / bps, startTime + 316.0 / bps, -45, timingWindow, level, false, false, true, true);

			addPlatform(startTime + 316.0 / bps, startTime + 318.0 / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 319.0 / bps, startTime + 320.5 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 321.5 / bps, startTime + 322.0 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 323.0 / bps, startTime + 323.0 / bps, 30, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 323.0 / bps, startTime + 324.0 / bps, 30, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 324.0 / bps, startTime + 324.0 / bps, 30, timingWindow, level, false, false, false, true);

			addPlatform(startTime + (292.0+32) / bps, startTime + (294.0+32) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (295.0+32) / bps, startTime + (295.5+32) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (295.5+32) / bps, startTime + (297.0+32) / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (297.0+32) / bps, startTime + (298.0+32) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (299.0+32) / bps, startTime + (300.0+32) / bps, -40, timingWindow, level, false, false, true, true);

			addPlatform(startTime + (300.0+32) / bps, startTime + (302.0+32) / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (303.0+32) / bps, startTime + (303.5+32) / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (303.5+32) / bps, startTime + (305.0+32) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (305.0+32) / bps, startTime + (306.0+32) / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (307.0+32) / bps, startTime + (307.0+32) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (307.0+32) / bps, startTime + (308.0+32) / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + (308.0+32) / bps, startTime + (308.0+32) / bps, 50, timingWindow, level, false, false, false, true);

			addPlatform(startTime + (308.0+32) / bps, startTime + (310.0+32) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (311.0+32) / bps, startTime + (311.5+32) / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (311.5+32) / bps, startTime + (313.0+32) / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (313.0+32) / bps, startTime + (314.0+32) / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (315.0+32) / bps, startTime + (316.0+32) / bps, -60, timingWindow, level, false, false, true, true);

			addPlatform(startTime + (316.0+32) / bps, startTime + (318.0+32) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (319.0+32) / bps, startTime + (320.5+32) / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (321.5+32) / bps, startTime + (322.0+32) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (323.0+32) / bps, startTime + (323.0+32) / bps, 45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (323.0+32) / bps, startTime + (324.0+32) / bps, 45, timingWindow, level, false, true, false, false);
			addPlatform(startTime + (324.0+32) / bps, startTime + (324.0+32) / bps, 45, timingWindow, level, false, false, false, true);
			
			addPlatform(startTime + (324.0+32) / bps, startTime + (324.0+32+32) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (324.0+32) / bps, startTime + (324.0+32+32) / bps, 45, timingWindow, level, false, false, true, true);
		}

		public static function LoadLevel5():void {
			var timingWindow:Number = 0.09;
			var level:int = 5;
			var startTime:Number = 0.0;
			var bps:Number = 90.0 / 60.0;
			
			GameWorld.world().respawnTime = startTime + 4.0 / bps;
			
			// Section 1
			GameWorld.world().add(new HueShift(startTime + 0.0 / bps, 90, false));
			addPlatform(startTime + -10.0 / bps, startTime + 8.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 9.0 / bps, startTime + 9.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 10.0 / bps, startTime + 10.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 10.5 / bps, startTime + 11.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 12.0 / bps, startTime + 12.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 12.0 / bps, startTime + 16.0 / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 16.0 / bps, startTime + 16.0 / bps, 50, timingWindow, level, false, false, false, true);

			addPlatform(startTime + 16.0 / bps, startTime + 16.75 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 17.25 / bps, startTime + 17.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 17.5 / bps, startTime + 19.0 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 19.0 / bps, startTime + 20.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 20.0 / bps, startTime + 20.75 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 21.25 / bps, startTime + 21.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 21.5 / bps, startTime + 23.0 / bps, -50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 23.0 / bps, startTime + 24.0 / bps, -50, timingWindow, level, false, true, false, false);
			
			addPlatform(startTime + (16.0+8) / bps, startTime + (16.0+8) / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + (16.0+8) / bps, startTime + (16.75+8) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (17.25+8) / bps, startTime + (17.5+8) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (17.5+8) / bps, startTime + (19.0+8) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (19.0+8) / bps, startTime + (20.0+8) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (20.0+8) / bps, startTime + (20.75+8) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (21.25+8) / bps, startTime + (21.5+8) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (21.5+8) / bps, startTime + (22.0+8) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (22.0+8) / bps, startTime + (24.0+8) / bps, 50, timingWindow, level, false, true, false, false);
			addPlatform(startTime + 32.0 / bps, startTime + 32.0 / bps, 50, timingWindow, level, false, false, false, true);

			// Section 2
			GameWorld.world().add(new Checkpoint(startTime + 32.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 32.0 / bps, 60, true));
			addPlatform(startTime + 32.0 / bps, startTime + 33.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 34.0 / bps, startTime + 34.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 34.5 / bps, startTime + 36.0 / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 36.0 / bps, startTime + 38.0 / bps, -60, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 38.0 / bps, startTime + 40.0 / bps, 60, timingWindow, level, false, true, true, false);
			addPlatform(startTime + 40.0 / bps, startTime + 40.0 / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 40.0 / bps, startTime + 41.5 / bps, -60, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 42.0 / bps, startTime + 42.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 42.0 / bps, startTime + 44.0 / bps, 55, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 44.0 / bps, startTime + 46.0 / bps, 55, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 46.0 / bps, startTime + 48.0 / bps, -55, timingWindow, level, false, true, true, false);
			addPlatform(startTime + 48.0 / bps, startTime + 48.0 / bps, -55, timingWindow, level, false, false, false, true);

			// Section 3
			addPlatform(startTime + 48.0 / bps, startTime + 49.5 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 50.0 / bps, startTime + 50.0 / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 50.0 / bps, startTime + 52.0 / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 52.5 / bps, startTime + 53.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 53.0 / bps, startTime + 54.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 54.0 / bps, startTime + 55.0 / bps, 50, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 55.0 / bps, startTime + 56.0 / bps, -50, timingWindow, level, false, true, true, false);

			addPlatform(startTime + 56.0 / bps, startTime + 56.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 56.5 / bps, startTime + 57.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 58.0 / bps, startTime + 58.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 58.0 / bps, startTime + 60.0 / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 60.0 / bps, startTime + 61.5 / bps, 50, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 61.5 / bps, startTime + 63.0 / bps, -50, timingWindow, level, false, true, true, false);
			addPlatform(startTime + 63.0 / bps, startTime + 63.0 / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 63.5 / bps, startTime + 63.5 / bps, -50, timingWindow, level, false, false, true, true);

			// Section 4
			GameWorld.world().add(new Checkpoint(startTime + 64.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 64.0 / bps, 30, true));
			addPlatform(startTime + 64.0 / bps, startTime + 70.5 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 70.5 / bps, startTime + 71.25 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 71.25 / bps, startTime + 72.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 72.0 / bps, startTime + 75.5 / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 75.5 / bps, startTime + (76.0 + 0.1) / bps, -50, timingWindow, level, false, false, true, true);

			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 79.0 / bps, -50));
			GameWorld.world().add(new HueShift(startTime + 80.0 / bps, -120, true));

			addPlatform(startTime + (79.0 - 0.1) / bps, startTime + 80.0 / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 80.5 / bps, startTime + 81.0 / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 81.5 / bps, startTime + 82.0 / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 82.5 / bps, startTime + 83.0 / bps, 45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 83.0 / bps, startTime + 84.0 / bps, 45, timingWindow, level, false, true, false, false);

			addPlatform(startTime + 84.0 / bps, startTime + 84.0 / bps, 45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 84.5 / bps, startTime + 85.0 / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 85.5 / bps, startTime + 86.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 86.0 / bps, startTime + 87.0 / bps, -40, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 87.0 / bps, startTime + 88.0 / bps, 40, timingWindow, level, false, true, true, false);

			addPlatform(startTime + 88.0 / bps, startTime + 88.0 / bps, 40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 88.0 / bps, startTime + 88.75 / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 89.25 / bps, startTime + 90.0 / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 90.0 / bps, startTime + 91.0 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 91.5 / bps, startTime + 92.0 / bps, 35, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 92.0 / bps, startTime + 93.0 / bps, 35, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 93.0 / bps, startTime + 94.0 / bps, -35, timingWindow, level, false, true, true, true);
			addPlatform(startTime + 94.0 / bps, startTime + 95.0 / bps, 35, timingWindow, level, false, true, true, true);
			addPlatform(startTime + 95.0 / bps, startTime + 96.0 / bps, -35, timingWindow, level, false, true, true, false);

			addPlatform(startTime + 96.0 / bps, startTime + 96.5 / bps, -35, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 97.0 / bps, startTime + 97.5 / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 97.5 / bps, startTime + 98.5 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 99.0 / bps, startTime + 99.5 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 99.5 / bps, startTime + 100.0 / bps, -35, timingWindow, level, false, false, true, false);

			addPlatform(startTime + 100.0 / bps, startTime + 101.0 / bps, -35, timingWindow, level, false, true, false, true);
			addPlatform(startTime + 101.0 / bps, startTime + 102.0 / bps, 35, timingWindow, level, false, true, true, false);
			addPlatform(startTime + 102.0 / bps, startTime + 102.0 / bps, 35, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 102.0 / bps, startTime + 102.5 / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 103.0 / bps, startTime + 103.0 / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform(startTime + 103.0 / bps, startTime + 104.0 / bps, -40, timingWindow, level, false, true, false, true);

			addPlatform(startTime + 104.0 / bps, startTime + 106.0 / bps, 40, timingWindow, level, false, true, true, true);
			addPlatform(startTime + 106.0 / bps, startTime + 108.0 / bps, -40, timingWindow, level, false, true, true, false);
			addPlatform(startTime + 108.0 / bps, startTime + 108.0 / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform(startTime + 108.5 / bps, startTime + 109.0 / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 109.5 / bps, startTime + 110.0 / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 110.0 / bps, startTime + 110.5 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 111.0 / bps, startTime + 111.0 / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + 111.0 / bps, startTime + 111.5 / bps, -35, timingWindow, level, false, false, true, true);

			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 112.0 / bps, -35));
			GameWorld.world().add(new HueShift(startTime + 112.0 / bps, 150, true));

			addPlatform(startTime + (32.0+80) / bps, startTime + (33.5+80) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (34.0+80) / bps, startTime + (34.0+80) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (34.5+80) / bps, startTime + (36.0+80) / bps, -45, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (36.0+80) / bps, startTime + (38.0+80) / bps, -45, timingWindow, level, false, true, false, true);
			addPlatform(startTime + (38.0+80) / bps, startTime + (40.0+80) / bps, 45, timingWindow, level, false, true, true, false);
			addPlatform(startTime + (40.0+80) / bps, startTime + (40.0+80) / bps, 45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + (40.0+80) / bps, startTime + (41.5+80) / bps, -45, timingWindow, level, false, false, false, true);
			addPlatform(startTime + (42.0+80) / bps, startTime + (42.0+80) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (42.0+80) / bps, startTime + (44.0+80) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (44.0+80) / bps, startTime + (46.0+80) / bps, 50, timingWindow, level, false, true, false, true);
			addPlatform(startTime + (46.0+80) / bps, startTime + (48.0+80) / bps, -50, timingWindow, level, false, true, true, false);
			addPlatform(startTime + (48.0+80) / bps, startTime + (48.0+80) / bps, -50, timingWindow, level, false, false, false, true);

			// Section 6
			addPlatform(startTime + (48.0+80) / bps, startTime + (49.5+80) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (50.0+80) / bps, startTime + (50.0+80) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (50.0+80) / bps, startTime + (52.0+80) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (52.5+80) / bps, startTime + (53.0+80) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (53.0+80) / bps, startTime + (54.0+80) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (54.0+80) / bps, startTime + (55.0+80) / bps, 50, timingWindow, level, false, true, false, true);
			addPlatform(startTime + (55.0+80) / bps, startTime + (56.0+80) / bps, -50, timingWindow, level, false, true, true, false);

			addPlatform(startTime + (56.0+80) / bps, startTime + (56.0+80) / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + (56.5+80) / bps, startTime + (57.5+80) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (58.0+80) / bps, startTime + (58.0+80) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (58.0+80) / bps, startTime + (60.0+80) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform(startTime + (60.0+80) / bps, startTime + (61.5+80) / bps, 50, timingWindow, level, false, true, false, true);
			addPlatform(startTime + (61.5+80) / bps, startTime + (63.0+80) / bps, -50, timingWindow, level, false, true, true, false);
			addPlatform(startTime + (63.0+80) / bps, startTime + (63.0+80) / bps, -50, timingWindow, level, false, false, false, true);
			addPlatform(startTime + (63.5+80) / bps, startTime + (63.5+80) / bps, -50, timingWindow, level, false, false, true, true);
			
			addPlatform(startTime + (144.0) / bps, startTime + (164.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform(startTime + (144.0) / bps, startTime + (164.0) / bps, 40, timingWindow, level, false, false, true, true);
		}
		
		public static function LoadLevel6():void {
			var timingWindow:Number = 0.1;
			var level:int = 6;
			var startTime:Number = 0.0;
			var bps:Number = 160.0 / 60.0;
			
			GameWorld.world().respawnTime = startTime + 18.0 / bps;
			
			// Section 1
			GameWorld.world().add(new HueShift(startTime + 0.0 / bps, -30, false));
			addPlatform((startTime + -10.0) / bps, (startTime + 25.0) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 26.0) / bps, (startTime + 26.0) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 27.0) / bps, (startTime + 28.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 29.0) / bps, (startTime + 29.0) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 30.0) / bps, (startTime + 31.0) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 32.0) / bps, (startTime + 32.0) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 33.0) / bps, (startTime + 35.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 35.0) / bps, (startTime + 37.0) / bps, 40, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 38.0) / bps, (startTime + 38.0) / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 39.0) / bps, (startTime + 40.0) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 41.0) / bps, (startTime + 41.0) / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 42.0) / bps, (startTime + 44.0) / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 44.0) / bps, (startTime + 45.0) / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 45.0) / bps, (startTime + 48.0) / bps, -40, timingWindow, level, false, true, false, false);

			// Section 2
			GameWorld.world().add(new Checkpoint(startTime + 48.0 / bps, -40));
			addPlatform((startTime + 48.0) / bps, (startTime + 56.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 57.0) / bps, (startTime + 57.0) / bps, -40, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 57.0) / bps, (startTime + 60.0) / bps, -40, timingWindow, level, false, true, false, false);
			addPlatform((startTime + 60.0) / bps, (startTime + 66.0) / bps, -40, timingWindow, level, false, false, false, false);
			addPlatform((startTime + 66.0) / bps, (startTime + 69.0) / bps, -40, timingWindow, level, false, true, false, false);
			addPlatform((startTime + 69.0) / bps, (startTime + 69.0) / bps, -40, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 70.0) / bps, (startTime + 70.0) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 71.0) / bps, (startTime + 71.0) / bps, -30, timingWindow, level, false, false, true, true);

			// Section 3
			GameWorld.world().add(new Checkpoint(startTime + 72.0 / bps, -30));
			GameWorld.world().add(new HueShift(startTime + 72.0 / bps, 60, true));
			addPlatform((startTime + 72.0) / bps, (startTime + 81.0) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 82.0) / bps, (startTime + 83.0) / bps, 30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 84.0) / bps, (startTime + 93.0) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 94.0) / bps, (startTime + 95.0) / bps, 30, timingWindow, level, false, false, true, true);

			// Section 4
			GameWorld.world().add(new Checkpoint(startTime + 96.0 / bps, -30));
			GameWorld.world().add(new HueShift(startTime + 96.0 / bps, 150, true));
			addPlatform((startTime + 95.0) / bps, (startTime + 98.0) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 99.0) / bps, (startTime + 99.0) / bps, -25, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 99.0) / bps, (startTime + 100.0) / bps, 25, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 101.0) / bps, (startTime + 101.0) / bps, 30, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 102.0) / bps, (startTime + 104.0) / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 105.0) / bps, (startTime + 105.0) / bps, 30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 105.0) / bps, (startTime + 106.0) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 107.0) / bps, (startTime + 107.0) / bps, -35, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 108.0) / bps, (startTime + 110.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 111.0) / bps, (startTime + 111.0) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 111.0) / bps, (startTime + 112.0) / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 113.0) / bps, (startTime + 113.0) / bps, 40, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 114.0) / bps, (startTime + 116.0) / bps, 45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 116.0) / bps, (startTime + 117.0) / bps, -45, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 117.0) / bps, (startTime + 120.0) / bps, -45, timingWindow, level, false, true, false, false);

			addPlatform((startTime + 120.0) / bps, (startTime + 122.0) / bps, -45, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 123.0) / bps, (startTime + 123.0) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 123.0) / bps, (startTime + 124.0) / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 125.0) / bps, (startTime + 125.0) / bps, 45, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 126.0) / bps, (startTime + 129.0) / bps, 50, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 129.0) / bps, (startTime + 132.0) / bps, 50, timingWindow, level, false, true, false, false);

			addPlatform((startTime + 132.0) / bps, (startTime + 132.0) / bps, 50, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 132.0) / bps, (startTime + 135.0) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 135.0) / bps, (startTime + 136.0) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 137.0) / bps, (startTime + 137.0) / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 138.0) / bps, (startTime + 138.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 138.0) / bps, (startTime + 141.0) / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 141.0) / bps, (startTime + 144.0) / bps, -60, timingWindow, level, false, true, false, false);

			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 144.0 / bps, -60));
			GameWorld.world().add(new HueShift(startTime + 144.0 / bps, 90, true));
			addPlatform((startTime + 144.0) / bps, (startTime + 151.0) / bps, -60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 152.0) / bps, (startTime + 152.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 153.0) / bps, (startTime + 155.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 155.0) / bps, (startTime + 163.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 164.0) / bps, (startTime + 164.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 165.0) / bps, (startTime + 165.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 165.0) / bps, (startTime + 166.0) / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 166.0) / bps, (startTime + 168.0) / bps, -60, timingWindow, level, false, true, false, false);

			// Section 5
			GameWorld.world().add(new Checkpoint(startTime + 168.0 / bps, -60));
			GameWorld.world().add(new HueShift(startTime + 168.0 / bps, 30, true));
			addPlatform((startTime + 168.0) / bps, (startTime + 171.0) / bps, -60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 172.0) / bps, (startTime + 172.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 172.0) / bps, (startTime + 173.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 174.0) / bps, (startTime + 176.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 177.0) / bps, (startTime + 177.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 177.0) / bps, (startTime + 178.0) / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 178.0) / bps, (startTime + 180.0) / bps, -60, timingWindow, level, false, true, false, false);

			addPlatform((startTime + 180.0) / bps, (startTime + 183.0) / bps, -60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 184.0) / bps, (startTime + 184.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 184.0) / bps, (startTime + 185.0) / bps, 60, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 186.0) / bps, (startTime + 187.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 188.0) / bps, (startTime + 188.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 189.0) / bps, (startTime + 189.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 189.0) / bps, (startTime + 191.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 191.0) / bps, (startTime + 192.0) / bps, 60, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 192.0) / bps, (startTime + 195.0) / bps, 60, timingWindow, level, false, true, false, false);
			addPlatform((startTime + 195.0) / bps, (startTime + 195.0) / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 196.0) / bps, (startTime + 198.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 199.0) / bps, (startTime + 200.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 200.0) / bps, (startTime + 201.0) / bps, 60, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 201.0) / bps, (startTime + 204.0) / bps, 60, timingWindow, level, false, true, false, false);

			addPlatform((startTime + 204.0) / bps, (startTime + 204.0) / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 205.0) / bps, (startTime + 205.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 205.0) / bps, (startTime + 207.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 208.0) / bps, (startTime + 208.0) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 208.0) / bps, (startTime + 209.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 210.0) / bps, (startTime + 210.0) / bps, 60, timingWindow, level, false, false, true, false);

			addPlatform((startTime + 210.0) / bps, (startTime + 213.0) / bps, 60, timingWindow, level, false, true, false, false);
			addPlatform((startTime + 213.0) / bps, (startTime + 213.0) / bps, 60, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 214.0) / bps, (startTime + 214.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 215.0) / bps, (startTime + 215.0) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 215.0) / bps, (startTime + 218.0) / bps, -60, timingWindow, level, false, false, true, true);

			// Section 6
			GameWorld.world().add(new Checkpoint(startTime + 216.0 / bps, -60));
			GameWorld.world().add(new HueShift(startTime + 216.0 / bps, 0, true));
			addPlatform((startTime + 27.0 + 192) / bps, (startTime + 29.0 + 192) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 30.0 + 192) / bps, (startTime + 32.0 + 192) / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 33.0 + 192) / bps, (startTime + 35.0 + 192) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 35.0 + 192) / bps, (startTime + 37.0 + 192) / bps, 60, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 38.0 + 192) / bps, (startTime + 38.0 + 192) / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 39.0 + 192) / bps, (startTime + 40.0 + 192) / bps, 50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 41.0 + 192) / bps, (startTime + 41.0 + 192) / bps, 55, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 42.0 + 192) / bps, (startTime + 44.0 + 192) / bps, 60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 44.0 + 192) / bps, (startTime + 45.0 + 192) / bps, -60, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 45.0 + 192) / bps, (startTime + 48.0 + 192) / bps, -60, timingWindow, level, false, true, false, false);
			
			addPlatform((startTime + 24.0 + 216) / bps, (startTime + 25.0 + 216) / bps, -60, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 26.0 + 216) / bps, (startTime + 26.0 + 216) / bps, -55, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 27.0 + 216) / bps, (startTime + 28.0 + 216) / bps, -50, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 29.0 + 216) / bps, (startTime + 29.0 + 216) / bps, -45, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 30.0 + 216) / bps, (startTime + 31.0 + 216) / bps, -40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 32.0 + 216) / bps, (startTime + 32.0 + 216) / bps, -35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 33.0 + 216) / bps, (startTime + 35.0 + 216) / bps, -30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 35.0 + 216) / bps, (startTime + 37.0 + 216) / bps, 30, timingWindow, level, false, false, true, true);

			addPlatform((startTime + 38.0 + 216) / bps, (startTime + 38.0 + 216) / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 39.0 + 216) / bps, (startTime + 40.0 + 216) / bps, 40, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 41.0 + 216) / bps, (startTime + 41.0 + 216) / bps, 35, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 42.0 + 216) / bps, (startTime + 44.0 + 216) / bps, 30, timingWindow, level, false, false, true, true);
			addPlatform((startTime + 44.0 + 216) / bps, (startTime + 45.0 + 216) / bps, -30, timingWindow, level, false, false, true, false);
			addPlatform((startTime + 45.0 + 216) / bps, (startTime + 48.0 + 216) / bps, -30, timingWindow, level, false, true, false, false);

			addPlatform((startTime + 48.0 + 216) / bps, (startTime + 68.0 + 216) / bps, -30, timingWindow, level, false, false, false, true);
			addPlatform((startTime + 48.0 + 216) / bps, (startTime + 68.0 + 216) / bps, 30, timingWindow, level, false, false, false, true);
		}
		
		private static function addPlatform(startT:Number, endT:Number, y:Number, timingWindow:Number, level:int, fake:Boolean = false, spiked:Boolean = false, useTimingBegin:Boolean = true, useTimingEnd:Boolean = true):void {
			
			var isReflection:Boolean = y > 0;
			var startX:Number;
			var endX:Number;
			
			var spikeWindow:Number = timingWindow;
			
			if (spiked) {
				startX = GetXFromTime(startT + spikeWindow, level);
				endX = GetXFromTime(endT - spikeWindow, level);
			} else {
				startX = GetXFromTime(startT - spikeWindow, level);
				endX = GetXFromTime(endT + spikeWindow, level);
				
			}
			if (useTimingBegin) {
				startX = GetXFromTime(startT - timingWindow, level);
			}
			if (useTimingEnd) {
				endX = GetXFromTime(endT + timingWindow, level);
				
			}
			GameWorld.world().add(new Platform(startX, endX, y, isReflection, endT, fake, spiked));
		}
	}
}
