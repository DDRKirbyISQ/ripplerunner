package {
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Particle;
	import net.flashpunk.graphics.TiledImage;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Platform extends Entity {
		
		[Embed(source='../img/platform_tile.png')]
		private static const kTileFile:Class;
		private var tile:TiledImage;

		[Embed(source='../img/platform_spike_tile.png')]
		private static const kSpikeTileFile:Class;
		private var spikeTile:TiledImage;
		
		private var ended:Boolean = false;
		private var endTime:Number;
		public var endX:Number;
		private var isReflection:Boolean;
		private var fakeEnd:Boolean;
		private var startX:Number;
		
		public var spiked:Boolean;
		
		public function Platform(startX:Number, endX:Number, y:Number, isReflection:Boolean, endTime:Number, fakeEnd:Boolean, spiked:Boolean = false) {
			var width:Number = endX - startX;
			spikeTile = new TiledImage(kSpikeTileFile, width, 10)
			tile = new TiledImage(kTileFile, width, 10)
			if (spiked) {
				addGraphic(spikeTile);
			} else {
				addGraphic(tile);
			}
			setHitbox(width, 10);
			this.x = startX;
			this.y = y;
			type = "block";
			name = "block";
			this.endX = endX;
			this.startX = startX;
			
			tile.originY = halfHeight;
			spikeTile.originY = halfHeight;
			this.isReflection = isReflection;
			this.spiked = spiked;
			
			if (isReflection) {
				tile.scaleY = -1;
				spikeTile.scaleY = -1;
				if (GameWorld.world() != null) {
					GameWorld.world().fxLayer.entities.add(this);
				} else {
					IntroWorld.world().fxLayer.entities.add(this);
				}
			} else {
				
			}
			
			this.fakeEnd = fakeEnd;
			
			this.endTime = endTime;
			
			originY = halfHeight;
		}
		
		override public function update():void {
			super.update();
			
			// Clear if we will never see this platform again.
			if (GameWorld.world() != null) {
				if (GameWorld.world().respawnTime > endTime + 5.0) {
					FP.world.remove(this);
				}
				
				if (Player.player().x > endX + FP.width || Player.player().x < startX - FP.width) {
					visible = false;
					collidable = false;
				} else {
					visible = true;
					collidable = true;
				}
				if (isReflection) {
					visible = false;
				}
			}
			
			if (fakeEnd) {
				return;
			}
			
			if (GameWorld.world().song.position < endTime - 0.5) {
				// reset to false for respawning
				ended = false;
			}
			
			if (GameWorld.world().song.position >= endTime && !ended) {
				ended = true;
				
				// Do particle effect.
				var particle:ParticleExplosion = GameWorld.world().create(ParticleExplosion) as ParticleExplosion;
				particle.init(endX, y);
			}
		}
	
	}

}