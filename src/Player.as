package {
	import flash.media.Sound;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Screen;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Text;
	import punk.fx.effects.AdjustFX;
	import punk.fx.graphics.FXSpritemap;
	import punk.fx.effects.GlitchFX;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Player extends Entity {
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:FXSpritemap = new FXSpritemap(kSpritemapFile, 30, 24);
		
		[Embed(source='../sfx/switch_level_1.mp3')]
		private static const kSwitch1File:Class;
		private var switch1Sfx:Sfx = new Sfx(kSwitch1File);
		
		[Embed(source='../sfx/jump_level_1.mp3')]
		private static const kJump1File:Class;
		private var jump1Sfx:Sfx = new Sfx(kJump1File);
		
		[Embed(source='../sfx/flip.mp3')]
		private static const kFlip1File:Class;

		// Whether you ARE the reflection
		private var isReflection:Boolean;
		
		// Whether you are active.
		public var isActive:Boolean;
		
		private static var playerInstance:Player;
		private static var reflectionInstance:Player;
		
		public var jumpVel:Number = -8;
		public var gravity:Number = .6;
		
		public var yVel:Number = 0;
		
		private var glitchFx:GlitchFX = new GlitchFX(5);
		private var adjustFx:AdjustFX = new AdjustFX();
		
		private var anythingTimer:int = 0;
		
		public var particleRate:int = 8;
		
		private var adjustFxTimer:int = 0;
		
		private var ctrlPressTimer:int = 0;
		
		public var flipped:Boolean = false;
		
		private var jumpQueue:int = 0;
		
		public function Player(x:Number, y:Number, isReflection:Boolean) {
			
			addAnimations();
			super(x, y, spritemap);
			
			setHitbox(16, 24);
			spritemap.originX = 16 / 2;
			spritemap.originY = isReflection ? 24 : 23;
			originX = halfWidth;
			originY = isReflection ? 0 : 24;
			if (isReflection) {
				reflectionInstance = this;
				type = "reflection";
				name = "reflection";
				spritemap.scaleY = -1;
			} else {
				playerInstance = this;
				type = "player";
				name = "player";
			}
			layer = -2100;
			
			if (isReflection) {
				isActive = false;
			} else {
				isActive = true;
			}
			this.isReflection = isReflection;
			
			spritemap.play("walk");
			
			spritemap.effects.add(adjustFx);
			spritemap.effects.add(glitchFx);
		}
		
		override public function update():void {
			super.update();
			
			if (GameWorld.world().paused) {
				return;
			}
			
			if (!isReflection && isActive) {
				Player.reflection().isActive = false;
			}
			if (!isReflection && !isActive) {
				Player.reflection().isActive = true;
			}
			
			// Set gravity according to level
			Songs.UpdateJumpGravity();
			
			anythingTimer++;
			
			// Update position.
			var songPos:Number = GameWorld.world().song.position;
			
			// Actually use averaged one.
			songPos = GameWorld.world().songPos;
			
			
			if (isReflection) {
				x = Songs.GetXFromTime(songPos, GameWorld.world().level);
			} else {
				x = reflection().x;
			}
			
			var colliding:Boolean = collide("block", x, y) != null;
			
			glitchFx.maxSlide = isActive ? 0 : 5;
			
			// Update y position.
			if (!isActive) {
				if (isReflection) {
					y = -player().y;
					yVel = -player().yVel;
				} else {
					y = -reflection().y;
					yVel = -reflection().yVel;
				}
			} else {
				// Handle y position for active.
				yVel += isReflection ? -gravity : gravity;
				
				// If we are already colliding, don't even bother checking.
				if (colliding) {
					y += yVel;
				} else {
					if (!tryMoveY(yVel)) {
						yVel = 0;
						player().spritemap.play("walk");
						reflection().spritemap.play("walk");
					}
				}
				
				// Check for death by falling.
				if (isReflection && y <= 0) {
					GameWorld.world().die();
				} else if (!isReflection && y >= 0) {
					GameWorld.world().die();
				}
			}
			
			if (Input.pressed("jump")) {
				jumpQueue = 6;
			}
			jumpQueue--;
			
			if ((Input.pressed("jump") || jumpQueue > 0) && !colliding && isGrounded() && !flipped && !Input.check("flip")) {
				yVel = isReflection ? -jumpVel : jumpVel;
				if (isActive) {
					new Sfx(kJump1File).play();
				}
				player().spritemap.play("jump");
				reflection().spritemap.play("jump");
			}
			
			if (adjustFx.contrast > 0) {
				adjustFx.contrast -= 0.1;
			}
			
			if (Input.pressed("swap")) {
				ctrlPressTimer = 0;
				// do swap.
				isActive = !isActive;
				if (isReflection) {
					new Sfx(kSwitch1File).play(0.75);
				}
				if (isActive) {
					adjustFx.contrast = 1;
					adjustFxTimer = 5;
					
					var particle:ParticleExplosion = GameWorld.world().create(ParticleExplosion) as ParticleExplosion;
					particle.init(x, y);
					
					// swap particles
					var rangeY:Number = Math.abs(player().y - reflection().y);
					var startY:Number = isReflection ? player().y : reflection().y;
					var endY:Number = !isReflection ? player().y : reflection().y;
					var incY:Number = isReflection ? rangeY / 10 : -rangeY / 10;
					for (var i:int = 0; i < 10; ++i) {
						var curY:Number = startY + incY * i;
						GameWorld.world().emitter3.emit("regular", GameWorld.kCameraX, curY);
					}
				}
			}
			
			if (isActive) {
				if (Input.check("flip") && isGrounded()) {
					// Be on other side.
					if (!flipped) {
						// play sfx
						new Sfx(kFlip1File).play();
					}
					
					flipped = true;
				}
				
				if (!Input.check("flip")) {
					if (flipped) {
						// play sfx
						new Sfx(kFlip1File).play();
					}
					
					flipped = false;
				}
			}
			
			if (isActive && isSpiked() && !flipped) {
				GameWorld.world().die();
			}
			
			if (!isActive) {
				flipped = isReflection ? player().flipped : reflection().flipped;
			}
			
			spritemap.scaleY = isReflection ? -1 : 1;
			spritemap.originY = isReflection ? 24 : 23;
			
			if (flipped) {
				spritemap.scaleY = -spritemap.scaleY;
				if (!isReflection) {
					spritemap.originY *= 1.5;
				} else {
					spritemap.originY *= 1.45;
				}
			}
			
			spritemap.alpha = isActive ? 1 : 0.4;
			
			if (isActive) {
				if (anythingTimer % particleRate == 0) {
					var arstY:Number = y + (isReflection ? 10 : -10);
					if (flipped) {
						arstY += isReflection ? -24 : 24;
					}
					GameWorld.world().emitter2.emit("regular", GameWorld.kCameraX, arstY);
				}
			}
		}
		
		public static function player():Player {
			return playerInstance;
		}
		
		public static function reflection():Player {
			return reflectionInstance;
		}
		
		private function addAnimations():void {
			// TODO: make walk anim faster at higher scroll speeds
			spritemap.add("walk", [0, 1, 2, 3, 4, 3, 2, 1], 0.2);
			spritemap.add("jump", [5], 0.2, false);
		}
		
		// Returns true if you were able to move, false if you collided.
		public function tryMoveY(amount:Number):Boolean {
			var increment:Number = amount > 0 ? 0.01 : -0.01;
			var progress:Number = 0;
			
			while (Math.abs(progress) < Math.abs(amount)) {
				if (Math.abs(progress) < Math.abs(amount) - Math.abs(increment)) {
					// Move by a bit.
					y += increment;
					progress += increment;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						y -= increment;
						return false;
					}
				} else {
					// Move by a bit.
					increment = amount - progress;
					y += amount - progress;
					progress += amount - progress;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						y -= increment;
						return false;
					}
				}
			}
			return true;
		}
		
		public function isGrounded():Boolean {
			var increment:Number = isReflection ? -1.0 : 1.0;
			
			y += increment;
			
			// If we are colliding, it means we're grounded.
			if (collide("block", x, y) != null) {
				y -= increment;
				return true;
			}
			
			y -= increment;
			
			return false;
		}
		
		public function isSpiked():Boolean {
			var increment:Number = isReflection ? -1.0 : 1.0;
			
			y += increment;
			
			// If we are colliding, it means we're grounded.
			var e:Entity = collide("block", x, y);
			if (e != null) {
				if ((e as Platform).spiked) {
					
					y -= increment;
					return true;
				}
				
			}
			
			y -= increment;
			
			return false;
		}
	}
}
