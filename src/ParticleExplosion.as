package {
	import net.flashpunk.FP
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Image;
	import flash.display.BitmapData;
	
	public class ParticleExplosion extends Entity {
		[Embed(source='../img/particle_tiny.png')]
		private const particleFile1:Class;
		[Embed(source='../img/particle_small.png')]
		private const particleFile2:Class;
		[Embed(source='../img/particle_medium.png')]
		private const particleFile3:Class;
		[Embed(source='../img/particle_large.png')]
		private const particleFile4:Class;
		
		private var emitter:Emitter;
		private var emitter2:Emitter;
		private var emitter3:Emitter;
		private var emitter4:Emitter;
		
		public function init(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
			emitter = new Emitter(particleFile1, 1, 1);
			emitter.newType("regular", [0]);
			emitter.setMotion("regular", 0, 0, 0, 360, 150, 45);
			emitter2 = new Emitter(particleFile2, 3, 3);
			emitter2.newType("regular", [0]);
			emitter2.setMotion("regular", 0, 0, 0, 360, 150, 45);
			emitter3 = new Emitter(particleFile3, 5, 5);
			emitter3.newType("regular", [0]);
			emitter3.setMotion("regular", 0, 0, 0, 360, 150, 45);
			emitter4 = new Emitter(particleFile4, 8, 8);
			emitter4.newType("regular", [0]);
			emitter4.setMotion("regular", 0, 0, 0, 360, 150, 45);

			emitter.setAlpha("regular", 1, 0);
			emitter2.setAlpha("regular", 1, 0);
			emitter3.setAlpha("regular", 1, 0);
			emitter4.setAlpha("regular", 1, 0);
			layer = -5000;
			this.graphic = emitter;
			addGraphic(emitter2);
			addGraphic(emitter3);
			addGraphic(emitter4);
			var i:int;
			for (i = 0; i < 30; ++i) {
				emitter.emit("regular", 0, 0);
			}
			for (i = 0; i < 30; ++i) {
				
				emitter2.emit("regular", 0, 0);
			}
			for (i = 0; i < 20; ++i) {
				emitter3.emit("regular", 0, 0);
			}
			for (i = 0; i < 10; ++i) {
				emitter4.emit("regular", 0, 0);
			}
		}
		
		override public function update():void {
			super.update();
			
			if (emitter.particleCount == 0 && emitter2.particleCount == 0 && emitter3.particleCount == 0 && emitter4.particleCount == 0) {
				FP.world.recycle(this);
			}
		}
	}

}