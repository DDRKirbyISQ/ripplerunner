package {
	import flash.display.DisplayObjectContainer;
	import flash.events.ActivityEvent;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Spritemap;
	
	import com.greensock.loading.data.ImageLoaderVars;
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Ease;
	import punk.fx.graphics.FXImage;
	import punk.fx.graphics.FXLayer;
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.AdjustFX;
	import net.flashpunk.graphics.Emitter;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class JukeboxWorld extends World {
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 59, 50);
		
		private static const kMenuStartY:int = 20;
		private static const kMenuHeight:int = 11;


		[Embed(source='../mus/stage1.mp3')]
		private static const kMus1File:Class;
		private static var mus1:Sfx = new Sfx(kMus1File);
		[Embed(source='../mus/stage2.mp3')]
		private static const kMus2File:Class;
		private static var mus2:Sfx = new Sfx(kMus2File);
		[Embed(source='../mus/stage3.mp3')]
		private static const kMus3File:Class;
		private static var mus3:Sfx = new Sfx(kMus3File);
		[Embed(source='../mus/intro.mp3')]
		private static const kMus4File:Class;
		private static var mus4:Sfx = new Sfx(kMus4File);
		[Embed(source='../mus/menu.mp3')]
		private static const kMus5File:Class;
		private static var mus5:Sfx = new Sfx(kMus5File);
		[Embed(source='../mus/stage4.mp3')]
		private static const kMus6File:Class;
		private static var mus6:Sfx = new Sfx(kMus6File);
		[Embed(source='../mus/stage5.mp3')]
		private static const kMus7File:Class;
		private static var mus7:Sfx = new Sfx(kMus7File);
		[Embed(source='../mus/stage6.mp3')]
		private static const kMus8File:Class;
		private static var mus8:Sfx = new Sfx(kMus8File);

        private var musics:Vector.<Sfx> = new <Sfx>[mus4, mus5, mus1, mus2, mus3, mus8, mus7, mus6];
		
		private var anythingTimer:int = 0;

        private var descriptions:Vector.<String> = new <String> [
"Make a Splash! (Intro)\n\
\n\
The little ditty that plays during the opening cutscene.  \
I didn't spend much time on this one, as I was approaching the deadline when I wrote it--just ripped both the instruments and melodies \
from the Stage 1 theme (yay motif reuse!) \
and turn it into a catchy short little jingle.\
",
"Watching the Tide (Menu)\n\
\n\
I usually try and make energetic and attention-grabbing menu themes, but I actually didn't have time to code up an exciting \
menu screen like I did for Hyper Furball (or Match Girl, to some extent).  I was thinking that I should just make something \
peaceful for the menu theme, so that's what I went with, evoking images of waves and waterdrops.  This was also one of the last \
things I did near the deadline, so I didn't really have the time to throw around--again, I started from the Stage 1 theme and just \
took bits and pieces of it, slowed it down, and there you go.  Hooray for more theme re-use too. :)\
",
"Ripple Runner (Stage 1)\n\
\n\
This was the first track I wrote for the game!  Being the first actual rhythm game I've made for Ludum Dare, I felt like \
composing the music was a very different experience.  The thing is that since you're working on the game while also developing the mechanics and coding up the level \
elements that go along with it, you actually end up switching off between composing and coding quite often.  I had to input \
all of the platform timings and obstacles in code based on the music timings, so I was constantly flipping back and forth between \
my audio sequencer and my IDE, trying to get things to match up right.  Because of this, I actually don't have accurate working time \
measurements for this soundtrack, as most of the time I spent with the project files was actually just me trying to look at beat numbers \
to map into code.  Anyhow, this song was great fun to write, and I definitely had the gameplay in mind first and foremost when I wrote \
it, making sure to keep space for tutorial messages and such.  It also starts off light and bouncy and then grows more intense \
and epic, which is exactly the sense I wanted to tell players \"Welcome to Ripple Runner!\" :D\
",
"Runner Meets Ripple (Stage 2)\n\
\n\
This one is an interesting one.  You can see that I'm really getting into a bit.trip runner style with the sound effects \
matching up to the game actions, which is *exactly* what I was going for when I conceptualized this game. :)  \
In the middle of the song I decided to do a section with long sustained chords and somehow I managed to replicate the \
Twoson theme from Earthbound, so...yeah, that's a small little cameo melody that hopefully will put a smile on \
the faces of some Earthbound/Mother fans who play this game.  Then it gets into a really funky and evil groove section, \
which I guess works fine since this is where you're starting to really get into the game and experiencing some difficulty for the first time.\
",
"Set Me Free (Stage 3)\n\
\n\
I was initially not planning on making this the final stage in the game, but unfortunately I didn't have enough time to \
do more than the 3 stages here even though I REALLY WANTED TO. =(  Regardless, I knew that I wanted a faster tempo to up \
the challenge level a bit, so this is at 140 BPM (the other two stages are at 120 BPM and 100 BPM).  \
Unfortunately I also didn't get to do much theme reuse this time around, so there's not as much continuity/cohesiveness \
with the soundtrack this time around.  I initially had grand plans to make a final stage where the music would use melodies \
from all of the previous stages...and maybe extra stages as well, for extra challenge...but alas, such is the world of Ludum Dare.  \
Perhaps I'll make a post-compo version with some more added features...^_^\
",
"Feelin the Groove (Stage 4)\n\
\n\
After making \"What Lies Beneath\", I decided that I could still use another song to bridge the gap between it and \"Set Me Free\", \
and I was thinking of what else I could do to offer a changeup in terms of song style.  I decided that something using triple meter or \
swing timing would be good, so I ended up writing this piece that I guess you could say is in 9/8 time?  I was actually quite happy \
with how I managed to correlate the musical elements with the gameplay for this one.  I also took this opportunity to really groove out \
with all of the solos and everything, so this one was definitely fun.  Hopefully it's a good stepping stone as far as difficulty goes...\
I tried to include some tricky segments, but also offer some time in between them, as opposed to the later stages which throw things at \
you more constantly.\
",
"What Lies Beneath (Stage 5)\n\
\n\
Here's the second song I made for Ripple Runner Deluxe.  I already had a bunch of fast songs, so I tried to make something \
with a slower tempo here for contrast.  This one's 90BPM, though I added more complex rhythms to increase the difficulty from that \
of Stage 3.  The main gimmick here is swapping/rippling between spiked platforms, and I featured that prominently in the level \
design here.  I tried to match it in the musical lines too, with call-and-response-type melodies.  I ended up with a pretty \
laid back piece, with quite a different texture than the others--there's a lot more echoing and ambience than with the other, \
more high-energy songs.\
",
"Take It to the Limit (Stage 6)\n\
\n\
This is the first song that I added to the post-compo version of Ripple Runner (Ripple Runner Deluxe).  \
I wanted to make something REALLY hard, partly just because I knew I wanted a really difficult challenge in my game \
for all of you expert rhythm-game players, but also to see if I needed to add another mechanic to up the difficulty.  \
This one is at a blistering 160 BPM, and well...even I have trouble clearing it without any mistakes.  \
I should note that there is one mechanic that I didn't yet use even in this stage--rippling from spike platform to inverted \
spike platform while holding the flip button.  So I might end up using that in some other stage.  \
But it seems like difficulty-wise this is fine for a final challenge.  It's certainly not impossible, but still challenging \
to about the extent that I want.\
"
];


		[Embed(source='../img/backdrop_1.png')]
		private static const kBackdrop1File:Class;
		[Embed(source='../img/backdrop_1r.png')]
		private static const kBackdrop1rFile:Class;
		[Embed(source='../img/backdrop_2.png')]
		private static const kBackdrop2File:Class;
		[Embed(source='../img/backdrop_2r.png')]
		private static const kBackdrop2rFile:Class;
		[Embed(source='../img/backdrop_3.png')]
		private static const kBackdrop3File:Class;
		[Embed(source='../img/backdrop_3r.png')]
		private static const kBackdrop3rFile:Class;

		[Embed(source='../img/backdrop_2_1.png')]
		private static const kBackdrop2_1File:Class;
		[Embed(source='../img/backdrop_2_1r.png')]
		private static const kBackdrop2_1rFile:Class;
		[Embed(source='../img/backdrop_2_2.png')]
		private static const kBackdrop2_2File:Class;
		[Embed(source='../img/backdrop_2_2r.png')]
		private static const kBackdrop2_2rFile:Class;
		[Embed(source='../img/backdrop_2_3.png')]
		private static const kBackdrop2_3File:Class;
		[Embed(source='../img/backdrop_2_3r.png')]
		private static const kBackdrop2_3rFile:Class;
		
		[Embed(source='../img/backdrop_3_1.png')]
		private static const kBackdrop3_1File:Class;
		[Embed(source='../img/backdrop_3_1r.png')]
		private static const kBackdrop3_1rFile:Class;
		[Embed(source='../img/backdrop_3_2.png')]
		private static const kBackdrop3_2File:Class;
		[Embed(source='../img/backdrop_3_2r.png')]
		private static const kBackdrop3_2rFile:Class;
		[Embed(source='../img/backdrop_3_3.png')]
		private static const kBackdrop3_3File:Class;
		[Embed(source='../img/backdrop_3_3r.png')]
		private static const kBackdrop3_3rFile:Class;

		[Embed(source='../img/backdrop_4_1.png')]
		private static const kBackdrop4_1File:Class;
		[Embed(source='../img/backdrop_4_1r.png')]
		private static const kBackdrop4_1rFile:Class;
		[Embed(source='../img/backdrop_4_2.png')]
		private static const kBackdrop4_2File:Class;
		[Embed(source='../img/backdrop_4_2r.png')]
		private static const kBackdrop4_2rFile:Class;
		[Embed(source='../img/backdrop_4_3.png')]
		private static const kBackdrop4_3File:Class;
		[Embed(source='../img/backdrop_4_3r.png')]
		private static const kBackdrop4_3rFile:Class;

		[Embed(source='../img/backdrop_5_1.png')]
		private static const kBackdrop5_1File:Class;
		[Embed(source='../img/backdrop_5_1r.png')]
		private static const kBackdrop5_1rFile:Class;
		[Embed(source='../img/backdrop_5_2.png')]
		private static const kBackdrop5_2File:Class;
		[Embed(source='../img/backdrop_5_2r.png')]
		private static const kBackdrop5_2rFile:Class;
		[Embed(source='../img/backdrop_5_3.png')]
		private static const kBackdrop5_3File:Class;
		[Embed(source='../img/backdrop_5_3r.png')]
		private static const kBackdrop5_3rFile:Class;
		
		[Embed(source='../img/backdrop_6_1.png')]
		private static const kBackdrop6_1File:Class;
		[Embed(source='../img/backdrop_6_1r.png')]
		private static const kBackdrop6_1rFile:Class;
		[Embed(source='../img/backdrop_6_2.png')]
		private static const kBackdrop6_2File:Class;
		[Embed(source='../img/backdrop_6_2r.png')]
		private static const kBackdrop6_2rFile:Class;
		[Embed(source='../img/backdrop_6_3.png')]
		private static const kBackdrop6_3File:Class;
		[Embed(source='../img/backdrop_6_3r.png')]
		private static const kBackdrop6_3rFile:Class;
		
		private var backdrop1:Backdrop = new Backdrop(kBackdrop1File, true, false);
		private var backdrop1r:Backdrop = new Backdrop(kBackdrop1rFile, true, false);
		private var backdrop2:Backdrop = new Backdrop(kBackdrop2File, true, false);
		private var backdrop2r:Backdrop = new Backdrop(kBackdrop2rFile, true, false);
		private var backdrop3:Backdrop = new Backdrop(kBackdrop3File, true, false);
		private var backdrop3r:Backdrop = new Backdrop(kBackdrop3rFile, true, false);
		private var backdrop2_1:Backdrop = new Backdrop(kBackdrop2_1File, true, false);
		private var backdrop2_1r:Backdrop = new Backdrop(kBackdrop2_1rFile, true, false);
		private var backdrop2_2:Backdrop = new Backdrop(kBackdrop2_2File, true, false);
		private var backdrop2_2r:Backdrop = new Backdrop(kBackdrop2_2rFile, true, false);
		private var backdrop2_3:Backdrop = new Backdrop(kBackdrop2_3File, true, false);
		private var backdrop2_3r:Backdrop = new Backdrop(kBackdrop2_3rFile, true, false);
		private var backdrop3_1:Backdrop = new Backdrop(kBackdrop3_1File, true, false);
		private var backdrop3_1r:Backdrop = new Backdrop(kBackdrop3_1rFile, true, false);
		private var backdrop3_2:Backdrop = new Backdrop(kBackdrop3_2File, true, false);
		private var backdrop3_2r:Backdrop = new Backdrop(kBackdrop3_2rFile, true, false);
		private var backdrop3_3:Backdrop = new Backdrop(kBackdrop3_3File, true, false);
		private var backdrop3_3r:Backdrop = new Backdrop(kBackdrop3_3rFile, true, false);
		private var backdrop4_1:Backdrop = new Backdrop(kBackdrop4_1File, true, false);
		private var backdrop4_1r:Backdrop = new Backdrop(kBackdrop4_1rFile, true, false);
		private var backdrop4_2:Backdrop = new Backdrop(kBackdrop4_2File, true, false);
		private var backdrop4_2r:Backdrop = new Backdrop(kBackdrop4_2rFile, true, false);
		private var backdrop4_3:Backdrop = new Backdrop(kBackdrop4_3File, true, false);
		private var backdrop4_3r:Backdrop = new Backdrop(kBackdrop4_3rFile, true, false);
		private var backdrop5_1:Backdrop = new Backdrop(kBackdrop5_1File, true, false);
		private var backdrop5_1r:Backdrop = new Backdrop(kBackdrop5_1rFile, true, false);
		private var backdrop5_2:Backdrop = new Backdrop(kBackdrop5_2File, true, false);
		private var backdrop5_2r:Backdrop = new Backdrop(kBackdrop5_2rFile, true, false);
		private var backdrop5_3:Backdrop = new Backdrop(kBackdrop5_3File, true, false);
		private var backdrop5_3r:Backdrop = new Backdrop(kBackdrop5_3rFile, true, false);
		private var backdrop6_1:Backdrop = new Backdrop(kBackdrop6_1File, true, false);
		private var backdrop6_1r:Backdrop = new Backdrop(kBackdrop6_1rFile, true, false);
		private var backdrop6_2:Backdrop = new Backdrop(kBackdrop6_2File, true, false);
		private var backdrop6_2r:Backdrop = new Backdrop(kBackdrop6_2rFile, true, false);
		private var backdrop6_3:Backdrop = new Backdrop(kBackdrop6_3File, true, false);
		private var backdrop6_3r:Backdrop = new Backdrop(kBackdrop6_3rFile, true, false);

		
		public var adjustFx:AdjustFX = new AdjustFX();
		
		
		[Embed(source='../img/water.png')]
		private static const kWaterFile:Class;
		private var water:Backdrop = new Backdrop(kWaterFile, true, false);
		[Embed(source='../img/water2.png')]
		private static const kWater2File:Class;
		private var water2:Backdrop = new Backdrop(kWater2File, true, false);
		
				public var fxLayer:FXLayer;


		[Embed(source='../img/cursor.png')]
		private static const kCursorFile:Class;
		private var cursor:Image = new Image(kCursorFile);

		[Embed(source='../img/flash.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		private var blackImage2:Image = new Image(kBlackImageFile);
		private var blackImage3:Image = new Image(kBlackImageFile);

		[Embed(source='../sfx/flip.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/switch_level_1.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var texts:Vector.<Text> = new Vector.<Text>();
		
		private var descriptionText:Text = new Text("", FP.halfWidth, FP.height - 188);

		private var selectedChoice:int = 0;

		private var fadeTimer:int = -1;

		private static const kFadeDuration:Number = 60;
		
		public static var instance:JukeboxWorld;
		
		private var descriptionShadow1:Text;
		private var descriptionShadow2:Text;
		private var descriptionShadow3:Text;
		private var descriptionShadow4:Text;

		// Returns you the GameWorld.  Valid even in its constructor.
		public static function world():JukeboxWorld {
			return instance as JukeboxWorld;
		}
		public function JukeboxWorld() {
			instance = this;
			FP.screen.color = 0xFFFFFF;
			
			var backdropFxLayer2 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer2.effects.add(new GlitchFX(2));
			addGraphic(backdropFxLayer2);
			
			var backdropFxLayer3 = new FXLayer(); // with no parameters will default to the size of the screen
			backdropFxLayer3.effects.add(new GlitchFX(3));
			addGraphic(backdropFxLayer3);
			
			var colorTransformFX:GlitchFX = new GlitchFX(5);
			fxLayer = new FXLayer(); // with no parameters will default to the size of the screen
			fxLayer.effects.add(colorTransformFX);
			addGraphic(fxLayer);
			
			var entity:Entity;
			backdrop1.scrollX = 0.05;
			backdrop1r.scrollX = 0.05;
			backdrop1r.y = FP.halfHeight;
			addGraphic(backdrop1, 1000);
			entity = new Entity(0, 0, backdrop1r);
			entity.layer = 1000;
			add(entity);
			backdrop2_1.scrollX = 0.05;
			backdrop2_1r.scrollX = 0.05;
			backdrop2_1r.y = FP.halfHeight;
			addGraphic(backdrop2_1, 1000);
			entity = new Entity(0, 0, backdrop2_1r);
			entity.layer = 1000;
			add(entity);
			backdrop3_1.scrollX = 0.05;
			backdrop3_1r.scrollX = 0.05;
			backdrop3_1r.y = FP.halfHeight;
			addGraphic(backdrop3_1, 1000);
			entity = new Entity(0, 0, backdrop3_1r);
			entity.layer = 1000;
			add(entity);
			backdrop4_1.scrollX = 0.05;
			backdrop4_1r.scrollX = 0.05;
			backdrop4_1r.y = FP.halfHeight;
			addGraphic(backdrop4_1, 1000);
			entity = new Entity(0, 0, backdrop4_1r);
			entity.layer = 1000;
			add(entity);
			backdrop5_1.scrollX = 0.05;
			backdrop5_1r.scrollX = 0.05;
			backdrop5_1r.y = FP.halfHeight;
			addGraphic(backdrop5_1, 1000);
			entity = new Entity(0, 0, backdrop5_1r);
			entity.layer = 1000;
			add(entity);
			backdrop6_1.scrollX = 0.05;
			backdrop6_1r.scrollX = 0.05;
			backdrop6_1r.y = FP.halfHeight;
			addGraphic(backdrop6_1, 1000);
			entity = new Entity(0, 0, backdrop6_1r);
			entity.layer = 1000;
			add(entity);
			
			backdrop2.scrollX = 0.15;
			backdrop2r.scrollX = 0.15;
			backdrop2r.y = FP.halfHeight;
			addGraphic(backdrop2, 1000);
			entity = new Entity(0, 0, backdrop2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			backdrop2_2.scrollX = 0.15;
			backdrop2_2r.scrollX = 0.15;
			backdrop2_2r.y = FP.halfHeight;
			addGraphic(backdrop2_2, 1000);
			entity = new Entity(0, 0, backdrop2_2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			backdrop3_2.scrollX = 0.15;
			backdrop3_2r.scrollX = 0.15;
			backdrop3_2r.y = FP.halfHeight;
			addGraphic(backdrop3_2, 1000);
			entity = new Entity(0, 0, backdrop3_2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			backdrop4_2.scrollX = 0.15;
			backdrop4_2r.scrollX = 0.15;
			backdrop4_2r.y = FP.halfHeight;
			addGraphic(backdrop4_2, 1000);
			entity = new Entity(0, 0, backdrop4_2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			backdrop5_2.scrollX = 0.15;
			backdrop5_2r.scrollX = 0.15;
			backdrop5_2r.y = FP.halfHeight;
			addGraphic(backdrop5_2, 1000);
			entity = new Entity(0, 0, backdrop5_2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			backdrop6_2.scrollX = 0.15;
			backdrop6_2r.scrollX = 0.15;
			backdrop6_2r.y = FP.halfHeight;
			addGraphic(backdrop6_2, 1000);
			entity = new Entity(0, 0, backdrop6_2r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer2.entities.add(entity);
			
			backdrop3.scrollX = 0.25;
			backdrop3r.scrollX = 0.25;
			backdrop3r.y = FP.halfHeight;
			addGraphic(backdrop3, 1000);
			entity = new Entity(0, 0, backdrop3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			backdrop2_3.scrollX = 0.25;
			backdrop2_3r.scrollX = 0.25;
			backdrop2_3r.y = FP.halfHeight;
			addGraphic(backdrop2_3, 1000);
			entity = new Entity(0, 0, backdrop2_3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			backdrop3_3.scrollX = 0.25;
			backdrop3_3r.scrollX = 0.25;
			backdrop3_3r.y = FP.halfHeight;
			addGraphic(backdrop3_3, 1000);
			entity = new Entity(0, 0, backdrop3_3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			backdrop4_3.scrollX = 0.25;
			backdrop4_3r.scrollX = 0.25;
			backdrop4_3r.y = FP.halfHeight;
			addGraphic(backdrop4_3, 1000);
			entity = new Entity(0, 0, backdrop4_3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			backdrop5_3.scrollX = 0.25;
			backdrop5_3r.scrollX = 0.25;
			backdrop5_3r.y = FP.halfHeight;
			addGraphic(backdrop5_3, 1000);
			entity = new Entity(0, 0, backdrop5_3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			backdrop6_3.scrollX = 0.25;
			backdrop6_3r.scrollX = 0.25;
			backdrop6_3r.y = FP.halfHeight;
			addGraphic(backdrop6_3, 1000);
			entity = new Entity(0, 0, backdrop6_3r);
			entity.layer = 1000;
			add(entity);
			backdropFxLayer3.entities.add(entity);
			
			backdrop1.alpha = 1;
			backdrop1r.alpha = 1;
			backdrop2.alpha = 1;
			backdrop2r.alpha = 1;
			backdrop3.alpha = 1;
			backdrop3r.alpha = 1;

			backdrop2_1.alpha = 0;
			backdrop2_1r.alpha = 0;
			backdrop2_2.alpha = 0;
			backdrop2_2r.alpha = 0;
			backdrop2_3.alpha = 0;
			backdrop2_3r.alpha = 0;
			backdrop3_1.alpha = 0;
			backdrop3_1r.alpha = 0;
			backdrop3_2.alpha = 0;
			backdrop3_2r.alpha = 0;
			backdrop3_3.alpha = 0;
			backdrop3_3r.alpha = 0;
			backdrop4_1.alpha = 0;
			backdrop4_1r.alpha = 0;
			backdrop4_2.alpha = 0;
			backdrop4_2r.alpha = 0;
			backdrop4_3.alpha = 0;
			backdrop4_3r.alpha = 0;
			backdrop5_1.alpha = 0;
			backdrop5_1r.alpha = 0;
			backdrop5_2.alpha = 0;
			backdrop5_2r.alpha = 0;
			backdrop5_3.alpha = 0;
			backdrop5_3r.alpha = 0;
			backdrop6_1.alpha = 0;
			backdrop6_1r.alpha = 0;
			backdrop6_2.alpha = 0;
			backdrop6_2r.alpha = 0;
			backdrop6_3.alpha = 0;
			backdrop6_3r.alpha = 0;
			
			addGraphic(water);
			addGraphic(water2);
			water2.scrollX = 0.8;
		
							var shadowEntity:Entity = new Entity();
							add(shadowEntity);

			
			var i:int = 0;
				for each (var choice:String in ["Make a Splash!", "Watching the Tide", "Ripple Runner", "Runner Meets Ripple", "Set Me Free", "Feelin the Groove", "What Lies Beneath", "Take It to the Limit", "Return to Main Menu"]) {
					var text:Text = new Text(choice, FP.halfWidth, kMenuStartY + i * kMenuHeight);
					text.originX = text.textWidth / 2;
					text.originY = text.textHeight / 2;
					text.scale = 0.5;
					text.scrollX = 0;
					texts.push(text);
				ShadowText.AddShadowText(text, shadowEntity, 1);
					i++;
					addGraphic(text, -20000);
				}
			
				var titleText:Text = new Text("~ Jukebox ~", FP.halfWidth, 5);
				titleText.originX = titleText.textWidth / 2;
				addGraphic(titleText, -10000);
				titleText.scale = 0.5;
				titleText.scrollX = 0;
				ShadowText.AddShadowText(titleText, shadowEntity, 1);
				
				var downloadText:Text = new Text("Download the soundtrack at http://ddrkirbyisq.bandcamp.com!");
				downloadText.originX = downloadText.textWidth / 2;
				downloadText.originY = downloadText.textHeight / 2;
				downloadText.x = FP.halfWidth;
				downloadText.y = 280;
				downloadText.color = 0xFFFF00;
				addGraphic(downloadText, -20000);
				downloadText.scale = 0.5;
				downloadText.scrollX = 0;
				ShadowText.AddShadowText(downloadText, shadowEntity, 1);

				var downloadText2:Text = new Text("_");
				downloadText2.originX = downloadText2.textWidth / 2;
				downloadText2.originY = downloadText2.textHeight / 2;
				downloadText2.x = FP.halfWidth + 78;
				downloadText2.y = 284;
				downloadText2.color = 0xFFFF00;
				addGraphic(downloadText2, -20000);
				downloadText2.scaleY = 0.5;
				downloadText2.scaleX = 17.25;
				downloadText2.scrollX = 0;
				ShadowText.AddShadowText(downloadText2, shadowEntity, 1);
				
				
				descriptionText.text = descriptions[0];
				descriptionText.wordWrap = true;
				descriptionText.width = 600;
				descriptionText.originX = 300;
				descriptionText.scale = 0.5;
				descriptionText.scrollX = 0;
				addGraphic(descriptionText, -10000);
				
				descriptionShadow1 = ShadowText.AddShadowText1(descriptionText, shadowEntity, 1);
				descriptionShadow2 = ShadowText.AddShadowText2(descriptionText, shadowEntity, 1);
				descriptionShadow3 = ShadowText.AddShadowText3(descriptionText, shadowEntity, 1);
				descriptionShadow4 = ShadowText.AddShadowText4(descriptionText, shadowEntity, 1);
						descriptionShadow1.wordWrap = true;
						descriptionShadow2.wordWrap = true;
						descriptionShadow3.wordWrap = true;
						descriptionShadow4.wordWrap = true;
						descriptionShadow1.width = 600;
						descriptionShadow2.width = 600;
						descriptionShadow3.width = 600;
						descriptionShadow4.width = 600;
						descriptionShadow1.originX = 300;
						descriptionShadow2.originX = 300;
						descriptionShadow3.originX = 300;
						descriptionShadow4.originX = 300;

			cursor.originX = cursor.width / 2;
			cursor.originY = cursor.height / 2;
			cursor.scrollX = 0;
			addGraphic(cursor, -10000);

			blackImage3.alpha = 0.25;
			addGraphic(blackImage3, -1000);
			blackImage3.color = 0x000000;
			blackImage3.scaleX = 400;
			blackImage3.scaleY = 300;
			blackImage3.scrollX = 0;
			
			blackImage.alpha = 1;
			addGraphic(blackImage, -30000);
			blackImage.scaleX = 400;
			blackImage.scaleY = 300;
			blackImage.scrollX = 0;
			
			var fxScreen:FXImage = new FXImage();
			fxScreen.effects.add(adjustFx);
			fxScreen.scrollX = 0;
			addGraphic(fxScreen, -15000);
			adjustFx.hue = FP.rand(11) * 30 - 180;
			targetHue = adjustFx.hue;
			
			anythingTimer += FP.rand(3600);
		}
		
		private var targetHue:int;

		private var backgroundIndex:int = 0;
		
		override public function update():void {
			super.update();

			anythingTimer++;
			
			FP.camera.x++;
			
			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
                for each (var musa:Sfx in musics) {
					if (musa.playing && musa.volume > fadeTimer / kFadeDuration) {
						musa.volume = fadeTimer / kFadeDuration;
					}
                }
				return;
			} else if (fadeTimer == 0) {
                for each (var musb:Sfx in musics) {
					musb.stop();
					}
					instance = null;
                FP.world = new MenuWorld();
				return;
			}
			
			if ((anythingTimer + 1) % (60 * 20) == 0) {
				targetHue = FP.rand(11) * 30 - 180;
			}
			
			if (adjustFx.hue > targetHue) {
				adjustFx.hue -= 1;
			} else if (adjustFx.hue < targetHue) {
				adjustFx.hue += 1;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;

			if (Input.mousePressed) {
				if (Input.mouseX > 185 && Input.mouseY > 270 && Input.mouseX < 335 && Input.mouseY < 290) {
					navigateToURL(new URLRequest("http://ddrkirbyisq.bandcamp.com/album/ripple-runner-original-soundtrack"));
				}
			}
			
				if (Input.pressed("menu")) {
					if (selectedChoice == texts.length - 1) {
						for each (var text:Text in texts) {
							text.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					} else {
						// Play selected song.
						if (musics[selectedChoice].playing) {
							musics[selectedChoice].stop();
							texts[selectedChoice].color = 0xffffff;
						} else {
							for each (var song:Sfx in musics) {
								song.stop();
							}
							musics[selectedChoice].loop();
							for each (var texta:Text in texts) {
								texta.color = 0xffffff;
							}
							texts[selectedChoice].color = 0xffff00;
							
							if (selectedChoice == 3) {
								backgroundIndex = 2;
							}
							else if (selectedChoice == 4) {
								backgroundIndex = 3;
							}
							else if (selectedChoice == 5) {
								backgroundIndex = 6;
							}
							else if (selectedChoice == 6) {
								backgroundIndex = 5;
							}
							else if (selectedChoice == 7) {
								backgroundIndex = 4;
							}
							else {
								backgroundIndex = 1;
							}
						}
					}
				}
			
				var rate:Number = 0.02;
				backdrop1.alpha -= rate;
				backdrop2.alpha -= rate;
				backdrop3.alpha -= rate;
				backdrop1r.alpha -= rate;
				backdrop2r.alpha -= rate;
				backdrop3r.alpha -= rate;
				backdrop2_1.alpha -= rate;
				backdrop2_2.alpha -= rate;
				backdrop2_3.alpha -= rate;
				backdrop2_1r.alpha -= rate;
				backdrop2_2r.alpha -= rate;
				backdrop2_3r.alpha -= rate;
				backdrop3_1.alpha -= rate;
				backdrop3_2.alpha -= rate;
				backdrop3_3.alpha -= rate;
				backdrop3_1r.alpha -= rate;
				backdrop3_2r.alpha -= rate;
				backdrop3_3r.alpha -= rate;
				backdrop4_1.alpha -= rate;
				backdrop4_2.alpha -= rate;
				backdrop4_3.alpha -= rate;
				backdrop4_1r.alpha -= rate;
				backdrop4_2r.alpha -= rate;
				backdrop4_3r.alpha -= rate;
				backdrop5_1.alpha -= rate;
				backdrop5_2.alpha -= rate;
				backdrop5_3.alpha -= rate;
				backdrop5_1r.alpha -= rate;
				backdrop5_2r.alpha -= rate;
				backdrop5_3r.alpha -= rate;
				backdrop6_1.alpha -= rate;
				backdrop6_2.alpha -= rate;
				backdrop6_3.alpha -= rate;
				backdrop6_1r.alpha -= rate;
				backdrop6_2r.alpha -= rate;
				backdrop6_3r.alpha -= rate;

			if (backgroundIndex == 2) {
				backdrop2_1.alpha += 2*rate;
				backdrop2_2.alpha += 2*rate;
				backdrop2_3.alpha += 2*rate;
				backdrop2_1r.alpha += 2*rate;
				backdrop2_2r.alpha += 2*rate;
				backdrop2_3r.alpha += 2*rate;
			}
			else if (backgroundIndex == 3) {
				backdrop3_1.alpha += 2*rate;
				backdrop3_2.alpha += 2*rate;
				backdrop3_3.alpha += 2*rate;
				backdrop3_1r.alpha += 2*rate;
				backdrop3_2r.alpha += 2*rate;
				backdrop3_3r.alpha += 2*rate;
				
			}
			else if (backgroundIndex == 4) {
				backdrop4_1.alpha += 2*rate;
				backdrop4_2.alpha += 2*rate;
				backdrop4_3.alpha += 2*rate;
				backdrop4_1r.alpha += 2*rate;
				backdrop4_2r.alpha += 2*rate;
				backdrop4_3r.alpha += 2*rate;
				
			}
			else if (backgroundIndex == 5) {
				backdrop5_1.alpha += 2*rate;
				backdrop5_2.alpha += 2*rate;
				backdrop5_3.alpha += 2*rate;
				backdrop5_1r.alpha += 2*rate;
				backdrop5_2r.alpha += 2*rate;
				backdrop5_3r.alpha += 2*rate;
				
			}
			else if (backgroundIndex == 6) {
				backdrop6_1.alpha += 2*rate;
				backdrop6_2.alpha += 2*rate;
				backdrop6_3.alpha += 2*rate;
				backdrop6_1r.alpha += 2*rate;
				backdrop6_2r.alpha += 2*rate;
				backdrop6_3r.alpha += 2*rate;
				
			}
			else {
				backdrop1.alpha += 2*rate;
				backdrop2.alpha += 2*rate;
				backdrop3.alpha += 2*rate;
				backdrop1r.alpha += 2*rate;
				backdrop2r.alpha += 2*rate;
				backdrop3r.alpha += 2*rate;
			}
				
			var oldChoice:int = selectedChoice;

			if (Input.pressed(Key.UP)) {
				cursorSfx.play();
				selectedChoice--;
			}
			if (Input.pressed(Key.DOWN)) {
				cursorSfx.play();
				selectedChoice++;
			}
			
				selectedChoice = (selectedChoice + texts.length) % texts.length;

				if (Input.pressed(Key.ESCAPE)) {
					if (selectedChoice != texts.length - 1) {
						cursorSfx.play();
						selectedChoice = texts.length - 1;
					} else {
						for each (var textw:Text in texts) {
							textw.color = 0xffffff;
						}
						// Return to main menu.
						confirmSfx.play();
						fadeTimer = kFadeDuration;
					}
				}
				var selectedText:Text = texts[selectedChoice];
				cursor.x = selectedText.x - 55;
				cursor.y = selectedText.y;
				
				if (selectedChoice < texts.length - 1) {
					if (oldChoice != selectedChoice) {
						descriptionText.text = descriptions[selectedChoice];
						descriptionShadow1.text = descriptionText.text;
						descriptionShadow2.text = descriptionText.text;
						descriptionShadow3.text = descriptionText.text;
						descriptionShadow4.text = descriptionText.text;
						descriptionShadow1.wordWrap = true;
						descriptionShadow2.wordWrap = true;
						descriptionShadow3.wordWrap = true;
						descriptionShadow4.wordWrap = true;
						descriptionShadow1.width = 600;
						descriptionShadow2.width = 600;
						descriptionShadow3.width = 600;
						descriptionShadow4.width = 600;
						descriptionShadow1.originX = 300;
						descriptionShadow2.originX = 300;
						descriptionShadow3.originX = 300;
						descriptionShadow4.originX = 300;
						descriptionShadow1.scrollX = 0;
						descriptionShadow2.scrollX = 0;
						descriptionShadow3.scrollX = 0;
						descriptionShadow4.scrollX = 0;
						descriptionText.wordWrap = true;
						descriptionText.width = 600;
						descriptionText.originX = 300;
					}
				} else {
					descriptionText.text = "";
						descriptionShadow1.text = descriptionText.text;
						descriptionShadow2.text = descriptionText.text;
						descriptionShadow3.text = descriptionText.text;
						descriptionShadow4.text = descriptionText.text;
				}
		}
		
	}
}
