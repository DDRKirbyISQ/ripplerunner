package {
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Entity;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class ShadowText {
		public static function AddShadowText1(text:Text, entity:Entity, amount:int):Text {
			return AddShadowTextAt(text, entity, -amount, 0);
		}
		public static function AddShadowText2(text:Text, entity:Entity, amount:int):Text {
			return AddShadowTextAt(text, entity, amount, 0);
		}
		public static function AddShadowText3(text:Text, entity:Entity, amount:int):Text {
			return AddShadowTextAt(text, entity, 0, -amount);
		}
		public static function AddShadowText4(text:Text, entity:Entity, amount:int):Text {
			return AddShadowTextAt(text, entity, 0, amount);
		}
		public static function AddShadowText(text:Text, entity:Entity, amount:int):void {
			AddShadowTextAt(text, entity, -amount, 0);
			AddShadowTextAt(text, entity, amount, 0);
			AddShadowTextAt(text, entity, 0, -amount);
			AddShadowTextAt(text, entity, 0, amount);
		}

		private static function AddShadowTextAt(text:Text, entity:Entity, x:int, y:int):Text {
			var newText:Text = new Text(text.text, text.x, text.y);
			newText.font = text.font;
			newText.color = 0x253712;
			newText.x += x;
			newText.y += y;
			newText.size = text.size;
			newText.scrollX = text.scrollX;
			newText.scrollY = text.scrollY;
			newText.originX = text.originX;
			newText.originY = text.originY;
			newText.scale = text.scale;
			newText.scaleX = text.scaleX;
			newText.scaleY = text.scaleY;
			entity.addGraphic(newText);
			return newText;
		}
	}
}
